import {
  GET_ANONYMOUS_USER_WISHLIST,
  GET_PRODUCT,
  GET_PRODUCTS,
  GET_USER_CART_PRODUCTS,
  GET_USER_WISHLIST,
} from '~/api/products';
import { CartItemModel, ProductModel } from '~/models/ProductModels';
import { ApolloCache, FetchResult } from '@apollo/client';

export const updateProductsCache = (
  cache: ApolloCache<any>,
  productId: string,
  isProductInWishlist: boolean,
  userId: string
): void => {
  try {
    const products: { products: ProductModel[] } | null = cache.readQuery<{
      products: ProductModel[];
    }>({
      query: GET_PRODUCTS,
      variables: {
        userId,
      },
    });

    if (!products || !products.products) {
      return;
    }

    const index = products.products.findIndex(
      (product: ProductModel) => product._id === productId
    );

    if (index === -1) {
      return;
    }

    const updatedProducts = [...products.products];
    updatedProducts[index].isProductInWishlist = isProductInWishlist;

    cache.writeQuery<{ products: ProductModel[] }>({
      query: GET_PRODUCTS,
      variables: {
        userId,
      },
      data: {
        products: updatedProducts,
      },
    });
  } catch (error) {
    console.error('Error in updateProductsCache:', error);
  }
};

export const updateAnonymousUserProductsCache = (
  cache: ApolloCache<any>,
  productId: string,
  isProductInWishlist: boolean
): void => {
  try {
    const products: { products: ProductModel[] } | null = cache.readQuery<{
      products: ProductModel[];
    }>({
      query: GET_PRODUCTS,
      variables: {
        userId: null,
      },
    });
    console.log(123, products);
    if (!products || !products.products) {
      return;
    }

    const index = products.products.findIndex(
      (product: ProductModel) => product._id === productId
    );

    if (index === -1) {
      return;
    }

    let updatedProducts = products.products.map((product, i) =>
      i === index
        ? { ...product, isProductInWishlist: isProductInWishlist }
        : product
    );

    cache.writeQuery<{ products: ProductModel[] }>({
      query: GET_PRODUCTS,
      variables: {
        userId: null,
      },
      data: {
        products: updatedProducts,
      },
    });
  } catch (error) {
    console.error('Error in updateAnonymousUserProductsCache:', error);
  }
};
export const anonymousUserWishlistCache = (
  cache: any,
  productId: any,
  productInWishlist: boolean
) => {
  const product = cache.readQuery({
    query: GET_PRODUCT,
    variables: { productId: productId, userId: null },
  });
  if (product) {
    const updatedProduct = {
      ...product.product,
      isProductInWishlist: productInWishlist,
    };
    cache.writeQuery({
      query: GET_PRODUCT,
      variables: { productId: productId, userId: null },
      data: {
        product: updatedProduct,
      },
    });
  }
};
export const deleteCartItemFromProductsCache = (
  cache: ApolloCache<any>,
  productId: string,
  isProductInCart: boolean,
  userId: string
): void => {
  try {
    const products: { products: ProductModel[] } | null = cache.readQuery<{
      products: ProductModel[];
    }>({
      query: GET_PRODUCTS,
      variables: {
        userId,
      },
    });

    if (!products || !products.products) {
      return;
    }

    const index = products.products.findIndex(
      (product: ProductModel) => product._id === productId
    );

    if (index === -1) {
      return;
    }

    const updatedProducts = [...products.products];
    updatedProducts[index].isProductInCart = isProductInCart;

    cache.writeQuery<{ products: ProductModel[] }>({
      query: GET_PRODUCTS,
      variables: {
        userId,
      },
      data: {
        products: updatedProducts,
      },
    });
  } catch (error) {
    console.error('Error in deleteCartItemFromProductsCache:', error);
  }
};

export const updateCartItemQuantityCache = (
  cache: ApolloCache<any>,
  productId: string,
  quantity: number,
  userId: string
): void => {
  try {
    const cartItems: { userCartItems: CartItemModel[] } | null =
      cache.readQuery<{ userCartItems: CartItemModel[] }>({
        query: GET_USER_CART_PRODUCTS,
        variables: {
          userId,
        },
      });

    console.log(123, cartItems);
    if (!cartItems || !cartItems.userCartItems) {
      return;
    }

    const index = cartItems.userCartItems.findIndex(
      (item: CartItemModel) => item.product._id === productId
    );
    if (index === -1) {
      return;
    }

    let updatedCartItems = [...cartItems.userCartItems];
    const updatedItem = { ...updatedCartItems[index], quantity: quantity };
    updatedCartItems[index] = updatedItem;

    cache.writeQuery<{ userCartItems: CartItemModel[] }>({
      query: GET_USER_CART_PRODUCTS,
      variables: {
        userId,
      },
      data: {
        userCartItems: updatedCartItems,
      },
    });
  } catch (error) {
    console.error('Error in updateCartItemQuantityCache:', error);
  }
};

export const deleteItemFromCartCache = (
  cache: ApolloCache<any>,
  productId: string,
  userId: string
): void => {
  try {
    const cartItems: { userCartItems: CartItemModel[] } | null =
      cache.readQuery<{ userCartItems: CartItemModel[] }>({
        query: GET_USER_CART_PRODUCTS,
        variables: {
          userId,
        },
      });

    if (!cartItems || !cartItems.userCartItems) {
      return;
    }

    const updatedCartItems = cartItems.userCartItems.filter(
      (item: CartItemModel) => item.product._id !== productId
    );

    cache.writeQuery<{ userCartItems: CartItemModel[] }>({
      query: GET_USER_CART_PRODUCTS,
      variables: {
        userId,
      },
      data: {
        userCartItems: updatedCartItems,
      },
    });
  } catch (error) {
    console.error('Error in deleteItemFromCartCache:', error);
  }
};

export const addItemToCartCache = (
  cache: ApolloCache<any>,
  cartItem: CartItemModel,
  userId: string
): void => {
  try {
    const cartItems: { userCartItems: CartItemModel[] } | null =
      cache.readQuery<{ userCartItems: CartItemModel[] }>({
        query: GET_USER_CART_PRODUCTS,
        variables: {
          userId,
        },
      });

    if (!cartItems || !cartItems.userCartItems) {
      return;
    }

    const existingCartItemIndex = cartItems.userCartItems.findIndex(
      (item) => item.product._id === cartItem.product._id
    );

    if (existingCartItemIndex !== -1) {
      if (cartItem.quantity > 1) {
        const updatedCartItems = cartItems.userCartItems.map((item, index) =>
          index === existingCartItemIndex
            ? { ...item, quantity: cartItem.quantity }
            : item
        );
        cache.writeQuery<{ userCartItems: CartItemModel[] }>({
          query: GET_USER_CART_PRODUCTS,
          variables: {
            userId,
          },
          data: {
            userCartItems: updatedCartItems,
          },
        });
      }
    } else {
      const updatedCartItems = [...cartItems.userCartItems, cartItem];
      cache.writeQuery<{ userCartItems: CartItemModel[] }>({
        query: GET_USER_CART_PRODUCTS,
        variables: {
          userId,
        },
        data: {
          userCartItems: updatedCartItems,
        },
      });
    }
  } catch (error) {
    console.error('Error in addItemToCartCache:', error);
  }
};

export const deleteItemFromWishlistCache = (
  cache: ApolloCache<any>,
  productId: string,
  userId: string
): void => {
  try {
    const existingData: { userWishlist: ProductModel[] } | null =
      cache.readQuery<{ userWishlist: ProductModel[] }>({
        query: GET_USER_WISHLIST,
        variables: {
          userId,
        },
      });

    if (!existingData || !existingData.userWishlist) {
      return;
    }

    const updatedWishlistItems = existingData.userWishlist.filter(
      (item: ProductModel) => item._id !== productId
    );

    cache.writeQuery<{ userWishlist: ProductModel[] }>({
      query: GET_USER_WISHLIST,
      variables: {
        userId,
      },
      data: {
        userWishlist: updatedWishlistItems,
      },
    });
  } catch (error) {
    console.error('Error in deleteItemFromWishlistCache:', error);
  }
};

export const addItemToWishlistCache = (
  cache: ApolloCache<any>,
  wishlistItem: ProductModel,
  userId: string
): void => {
  try {
    const existingData: { userWishlist: ProductModel[] } | null =
      cache.readQuery<{ userWishlist: ProductModel[] }>({
        query: GET_USER_WISHLIST,
        variables: {
          userId,
        },
      });

    if (!existingData || !existingData.userWishlist) {
      return;
    }

    const updatedWishlistItems = [...existingData.userWishlist, wishlistItem];

    cache.writeQuery<{ userWishlist: ProductModel[] }>({
      query: GET_USER_WISHLIST,
      variables: {
        userId,
      },
      data: {
        userWishlist: updatedWishlistItems,
      },
    });
  } catch (error) {
    console.error('Error in addItemToWishlistCache:', error);
  }
};

export const deleteProductCache = (
  cache: ApolloCache<any>,
  productId: string,
  userId: string
): void => {
  try {
    const existingData: { products: ProductModel[] } | null = cache.readQuery({
      query: GET_PRODUCTS,
      variables: {
        userId,
      },
    });

    if (!existingData) {
      return;
    }

    const updatedProducts = filterOutProductById(
      existingData.products,
      productId
    );

    cache.writeQuery({
      query: GET_PRODUCTS,
      variables: {
        userId,
      },
      data: {
        products: updatedProducts,
      },
    });
  } catch (error) {
    console.error('Error in deleteProductCache:', error);
  }
};

export const addProductCache = (
  cache: ApolloCache<any>,
  product: ProductModel,
  userId: string
): void => {
  try {
    const existingData: { products: ProductModel[] } | null = cache.readQuery({
      query: GET_PRODUCTS,
      variables: {
        userId,
      },
    });
    console.log('existingData', existingData);

    if (!existingData) {
      const newData = { products: [product] };
      cache.writeQuery({
        query: GET_PRODUCTS,
        variables: {
          userId,
        },
        data: newData,
      });
      console.log('addProductCache', newData);
      return;
    }

    const updatedProducts = [...existingData.products, product];

    cache.writeQuery({
      query: GET_PRODUCTS,
      variables: {
        userId,
      },
      data: {
        products: updatedProducts,
      },
    });
  } catch (error) {
    console.error('Error in addProductCache:', error);
  }
};

const filterOutProductById = (
  products: ProductModel[],
  productId: string
): ProductModel[] => {
  return products.filter((item: ProductModel) => item._id !== productId);
};
