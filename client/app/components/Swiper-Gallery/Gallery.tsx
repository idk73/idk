import React from 'react';
import Swiper from 'swiper';

// import 'swiper';
import 'swiper/swiper.min.css';
const Gallery = ({ images }) => {
  React.useEffect(() => {
    const swiperCards = new Swiper('.gallery-cards', {
      loop: true,
      loopedSlides: 5,
      cssMode: true,
      effect: 'fade',
    });

    const swiperThumbs = new Swiper('.gallery-thumbs', {
      loop: true,
      loopedSlides: 5,
      slidesPerView: 3,
      centeredSlides: true,
      slideToClickedSlide: true,

      pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

    swiperThumbs.controller.control = swiperCards;
  }, []);

  return (
    <div className="gallery">
      {/* SWIPER GALLERY CARDS */}
      <div className="swiper gallery-cards">
        <div className="swiper-wrapper">
          {images.map((imageUrl: string, index: string) => (
            <div className="swiper-slide" key={index}>
              <article className="gallery__card">
                <img
                  src={imageUrl}
                  alt="image gallery"
                  className="gallery__img"
                />
                <div className="gallery__data">
                  <h3 className="gallery__title">Card Title</h3>
                  <span className="gallery__subtitle">Card Subtitle</span>
                </div>
              </article>
            </div>
          ))}
        </div>
      </div>

      {/* SWIPER GALLERY THUMBNAIL */}
      <div className="gallery__overflow">
        <div className="swiper gallery-thumbs">
          <div className="swiper-wrapper">
            {images.map((imageUrl: string, index: string) => (
              <div className="swiper-slide" key={index}>
                <div className="gallery__thumbnail">
                  <img
                    src={imageUrl}
                    alt="image thumbnail"
                    className="gallery__thumbnail-img"
                  />
                </div>
              </div>
            ))}
          </div>

          {/* Swiper pagination */}
          <div className="swiper-pagination"></div>
        </div>

        {/* Swiper arrows */}
        <div className="swiper-button-next">
          <i className="ri-arrow-right-line"></i>
        </div>
        <div className="swiper-button-prev">
          <i className="ri-arrow-left-line"></i>
        </div>
      </div>
    </div>
  );
};

export default Gallery;
