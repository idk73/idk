const Success = () => {
    return ( 
        <h1 className="font-semibold text-lg text-center mt-4">Thank you for your purchase!</h1>
     );
}
 
export default Success;