var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf, __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
};
var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
  isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: !0 }) : target,
  mod
)), __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);

// <stdin>
var stdin_exports = {};
__export(stdin_exports, {
  assets: () => assets_manifest_default,
  assetsBuildDirectory: () => assetsBuildDirectory,
  entry: () => entry,
  future: () => future,
  publicPath: () => publicPath,
  routes: () => routes
});
module.exports = __toCommonJS(stdin_exports);

// app/entry.server.tsx
var entry_server_exports = {};
__export(entry_server_exports, {
  default: () => handleRequest
});
var import_react = require("@remix-run/react"), import_server = require("react-dom/server"), import_client = require("@apollo/client"), import_ssr = require("@apollo/client/react/ssr");

// app/context/accessToken.ts
var accessToken = "", setAccessToken = (token) => {
  accessToken = token;
}, getAccessToken = () => accessToken, refreshToken = async () => {
  try {
    let response = await fetch("http://localhost:10/refresh_token", {
      method: "POST",
      credentials: "include"
    }), data = await response.json();
    if (response.ok)
      return setAccessToken(data.accessToken), data;
    setAccessToken("");
  } catch (error) {
    console.error(error);
  }
};

// app/entry.server.tsx
var import_jsx_dev_runtime = require("react/jsx-dev-runtime");
function handleRequest(request, responseStatusCode, responseHeaders, remixContext) {
  let token = getAccessToken(), client = new import_client.ApolloClient({
    ssrMode: !0,
    cache: new import_client.InMemoryCache(),
    link: (0, import_client.createHttpLink)({
      uri: "http://localhost:5000/graphql",
      credentials: request.credentials ?? "include",
      headers: {
        Authorization: token !== "" ? "Bearer " + token : ""
      }
    })
  }), App2 = /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(import_client.ApolloProvider, { client, children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(import_react.RemixServer, { context: remixContext, url: request.url }, void 0, !1, {
    fileName: "app/entry.server.tsx",
    lineNumber: 34,
    columnNumber: 7
  }, this) }, void 0, !1, {
    fileName: "app/entry.server.tsx",
    lineNumber: 33,
    columnNumber: 5
  }, this);
  return (0, import_ssr.getDataFromTree)(App2).then(() => {
    let initialState = client.extract(), markup = (0, import_server.renderToString)(
      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(import_jsx_dev_runtime.Fragment, { children: [
        App2,
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(
          "script",
          {
            dangerouslySetInnerHTML: {
              __html: `window.__APOLLO_STATE__=${JSON.stringify(
                initialState
              ).replace(/</g, "\\u003c")}`
            }
          },
          void 0,
          !1,
          {
            fileName: "app/entry.server.tsx",
            lineNumber: 43,
            columnNumber: 9
          },
          this
        )
      ] }, void 0, !0, {
        fileName: "app/entry.server.tsx",
        lineNumber: 41,
        columnNumber: 7
      }, this)
    );
    return responseHeaders.set("Content-Type", "text/html"), new Response("<!DOCTYPE html>" + markup, {
      status: responseStatusCode,
      headers: responseHeaders
    });
  });
}

// app/root.tsx
var root_exports = {};
__export(root_exports, {
  default: () => App,
  links: () => links,
  meta: () => meta
});
var import_react5 = require("@remix-run/react");

// app/context/auth-context.js
var import_react2 = __toESM(require("react")), auth_context_default = import_react2.default.createContext({
  userId: null,
  token: null,
  login: (token, userId, tokenExpiration, role) => {
  },
  logout: () => {
  },
  role: null
});

// app/components/Navbar/Navbar.tsx
var import_react3 = require("@remix-run/react");
var import_client3 = require("@apollo/client");
var import_react4 = require("react");

// app/api/products.ts
var import_client2 = require("@apollo/client"), GET_PRODUCTS = import_client2.gql`
  query Query($userId: ID) {
    products(userId: $userId) {
      _id
      name
      description
      price
      status
      category
      images
      isProductInWishlist
      isProductInCart
      user {
        _id
        email
      }
    }
  }
`, SEARCH_PRODUCTS = import_client2.gql`
  query Query($query: String!) {
    searchProducts(query: $query) {
      _id
      name
      description
      price
      status
      category
      images
      isProductInWishlist
      isProductInCart
    }
  }
`, GET_PRODUCT = import_client2.gql`
  query Query($productId: ID!, $userId: ID) {
    product(productId: $productId, userId: $userId) {
      _id
      category
      description
      images
      name
      price
      status
      isProductInWishlist
      isProductInCart
    }
  }
`, ADD_PRODUCT = import_client2.gql`
  mutation Mutation($productInput: ProductInput) {
    addProduct(productInput: $productInput) {
      name
      description
      price
      category
      status
      images
      user {
        _id
        email
      }
    }
  }
`, ADD_PRODUCT_TO_WISHLIST = import_client2.gql`
  mutation Mutation($productId: ID!, $userId: ID!) {
    addToWishlist(productId: $productId, userId: $userId) {
      _id
      name
      description
      isProductInWishlist
    }
  }
`, REMOVE_PRODUCT_FROM_WISHLIST = import_client2.gql`
  mutation Mutation($productId: ID!, $userId: ID!) {
    removeFromWishlist(productId: $productId, userId: $userId) {
      _id
      name
      description
      isProductInWishlist
    }
  }
`, ADD_PRODUCT_TO_CART = import_client2.gql`
  mutation Mutation($productId: ID!, $userId: ID!) {
    addToCart(productId: $productId, userId: $userId) {
      product {
        _id
        name
        description
        price
        isProductInCart
      }
      quantity
    }
  }
`, REMOVE_PRODUCT_FROM_CART = import_client2.gql`
  mutation Mutation($productId: ID!, $userId: ID!) {
    removeFromCart(productId: $productId, userId: $userId) {
      product {
        _id
        name
        description
        price
        isProductInCart
      }
      quantity
    }
  }
`, DELETE_PRODUCT_FROM_CART = import_client2.gql`
  mutation Mutation($productId: ID!, $userId: ID!) {
    deleteFromCart(productId: $productId, userId: $userId)
  }
`, DELETE_PRODUCT = import_client2.gql`
  mutation Mutation($productId: ID!, $userId: ID!) {
    deleteProduct(productId: $productId, userId: $userId)
  }
`, GET_USER_CART_PRODUCTS = import_client2.gql`
  query UserCartItems($userId: ID!) {
    userCartItems(userId: $userId) {
      product {
        _id
        name
        description
        price
        images
        category
        status
      }
      quantity
    }
  }
`, GET_USER_WISHLIST = import_client2.gql`
  query UserWishlist($userId: ID!) {
    userWishlist(userId: $userId) {
      _id
      name
      description
      price
      images
      category
      status
      isProductInWishlist
    }
  }
`, GET_ANONYMOUS_USER_WISHLIST = import_client2.gql`
  query GetProductsByIds($productIds: [ID!]!) {
    getProductsByIds(productIds: $productIds) {
      _id
      name
      isProductInWishlist
    }
  }
`;

// app/components/Navbar/Navbar.tsx
var import_jsx_dev_runtime2 = require("react/jsx-dev-runtime"), Navbar = () => {
  let context = (0, import_react4.useContext)(auth_context_default), client = (0, import_client3.useApolloClient)(), navigate = (0, import_react3.useNavigate)(), [cartItemsCount, setCartItemsCount] = (0, import_react4.useState)(0), [searchQuery, setSearchQuery] = (0, import_react4.useState)(""), [isSearchOpen, setIsSearchOpen] = (0, import_react4.useState)(!1), [isBurgerOpen, setIsBurgerOpen] = (0, import_react4.useState)(!1);
  (0, import_react4.useEffect)(() => {
    if (context && context.userId) {
      let subscription = client.watchQuery({
        query: GET_USER_CART_PRODUCTS,
        variables: {
          userId: context.userId
        }
      }).subscribe((res) => {
        var _a;
        let cartItems = ((_a = res == null ? void 0 : res.data) == null ? void 0 : _a.userCartItems) || [];
        setCartItemsCount(cartItems.length), console.log(333, cartItems);
      });
      return () => {
        subscription.unsubscribe();
      };
    }
  }, [context, client]);
  let handleSearchIconClick = () => {
    setIsSearchOpen(!isSearchOpen);
  }, handleBurgerToggle = () => {
    setIsBurgerOpen(!isBurgerOpen);
  }, handleFormSubmit = (event) => {
    event.preventDefault();
    let url = `/search?query=${searchQuery}`;
    navigate(url);
  };
  return /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(auth_context_default.Consumer, { children: (context2) => /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "flex flex-wrap place-items-start", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("section", { className: "relative mx-auto w-full", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("nav", { className: "flex justify-between bg-rose-900 text-white w-full", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "px-5 xl:px-12 py-6 flex w-full items-center", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
        "a",
        {
          className: "z-[1035] navbar-burger self-center cursor-pointer mr-12 lg:hidden",
          onClick: handleBurgerToggle,
          children: [
            isBurgerOpen && /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
              "svg",
              {
                xmlns: "http://www.w3.org/2000/svg",
                viewBox: "0 0 24 24",
                className: "h-5 w-5",
                children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                  "path",
                  {
                    fill: "currentColor",
                    d: "M17.6 6.6l-4.6 4.6 4.6 4.6-1.4 1.4-4.6-4.6-4.6 4.6-1.4-1.4 4.6-4.6-4.6-4.6 1.4-1.4 4.6 4.6 4.6-4.6z"
                  },
                  void 0,
                  !1,
                  {
                    fileName: "app/components/Navbar/Navbar.tsx",
                    lineNumber: 70,
                    columnNumber: 27
                  },
                  this
                )
              },
              void 0,
              !1,
              {
                fileName: "app/components/Navbar/Navbar.tsx",
                lineNumber: 65,
                columnNumber: 25
              },
              this
            ),
            !isBurgerOpen && /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
              "svg",
              {
                xmlns: "http://www.w3.org/2000/svg",
                viewBox: "0 0 24 24",
                fill: "currentColor",
                className: "h-5 w-5",
                children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                  "path",
                  {
                    fillRule: "evenodd",
                    d: "M3 6.75A.75.75 0 013.75 6h16.5a.75.75 0 010 1.5H3.75A.75.75 0 013 6.75zM3 12a.75.75 0 01.75-.75h16.5a.75.75 0 010 1.5H3.75A.75.75 0 013 12zm0 5.25a.75.75 0 01.75-.75h16.5a.75.75 0 010 1.5H3.75a.75.75 0 01-.75-.75z",
                    clipRule: "evenodd"
                  },
                  void 0,
                  !1,
                  {
                    fileName: "app/components/Navbar/Navbar.tsx",
                    lineNumber: 83,
                    columnNumber: 27
                  },
                  this
                )
              },
              void 0,
              !1,
              {
                fileName: "app/components/Navbar/Navbar.tsx",
                lineNumber: 77,
                columnNumber: 25
              },
              this
            )
          ]
        },
        void 0,
        !0,
        {
          fileName: "app/components/Navbar/Navbar.tsx",
          lineNumber: 60,
          columnNumber: 21
        },
        this
      ),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
          "svg",
          {
            fill: "#000000",
            width: "64px",
            height: "64px",
            viewBox: "-3.2 -3.2 38.40 38.40",
            id: "icon",
            xmlns: "http://www.w3.org/2000/svg",
            stroke: "#000000",
            transform: "rotate(0)",
            children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("g", { id: "SVGRepo_bgCarrier", strokeWidth: "0", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                "path",
                {
                  transform: "translate(-3.2, -3.2), scale(1.2)",
                  d: "M16,29.11410644816028C18.3611321764303,29.597335456081023,21.22195452118116,29.781109444082883,23.049255225393505,28.209668205901952C24.912117665805187,26.607644601810406,23.760772167111302,23.311849799068106,25.12393680893047,21.267707372705143C26.635693376615126,19.000742955103263,31.06211911615978,18.721729480472398,31.191441294219757,16C31.317967084277647,13.337123624063006,27.01525016564503,12.718079379608293,25.638643714259395,10.435126456949448C24.302232684135127,8.218833293406048,25.736582524801527,4.304748205167345,23.43070005708271,3.1296499653277987C21.071990446252705,1.9276305429237632,18.633983950721127,5.257687753568808,16,5.5231877863407135C13.744370448046723,5.750550492860211,11.596907434154883,4.0411571794051735,9.386605713930399,4.54526508604174C6.9025596600533845,5.1118064069622084,4.442009761654079,6.394834266017133,2.992149615064916,8.489914078679343C1.513036695320317,10.627264792191209,0.4116903500631133,13.621348600310169,1.4595742275317498,15.999999999999996C2.5725631988665043,18.52643716677852,6.384370015738386,18.445539984427572,8.267369370592185,20.464436375432538C9.56339980004111,21.854001990472383,9.17417247433633,24.212573950384154,10.44059009270535,25.629180419536105C11.923534995829877,27.287992893945308,13.82014816045032,28.66797825069852,16,29.11410644816028",
                  fill: "#ffffff",
                  strokeWidth: "0"
                },
                void 0,
                !1,
                {
                  fileName: "app/components/Navbar/Navbar.tsx",
                  lineNumber: 104,
                  columnNumber: 27
                },
                this
              ) }, void 0, !1, {
                fileName: "app/components/Navbar/Navbar.tsx",
                lineNumber: 103,
                columnNumber: 25
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                "g",
                {
                  id: "SVGRepo_tracerCarrier",
                  strokeLinecap: "round",
                  strokeLinejoin: "round",
                  stroke: "#CCCCCC",
                  strokeWidth: "0.192"
                },
                void 0,
                !1,
                {
                  fileName: "app/components/Navbar/Navbar.tsx",
                  lineNumber: 111,
                  columnNumber: 25
                },
                this
              ),
              /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("g", { id: "SVGRepo_iconCarrier", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("defs", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("style", {}, void 0, !1, {
                  fileName: "app/components/Navbar/Navbar.tsx",
                  lineNumber: 120,
                  columnNumber: 29
                }, this) }, void 0, !1, {
                  fileName: "app/components/Navbar/Navbar.tsx",
                  lineNumber: 119,
                  columnNumber: 27
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("polygon", { points: "8 11 11 11 11 23 13 23 13 11 16 11 16 9 8 9 8 11" }, void 0, !1, {
                  fileName: "app/components/Navbar/Navbar.tsx",
                  lineNumber: 122,
                  columnNumber: 27
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("path", { d: "M23,15V13H20V11H18v2H16v2h2v6a2,2,0,0,0,2,2h3V21H20V15Z" }, void 0, !1, {
                  fileName: "app/components/Navbar/Navbar.tsx",
                  lineNumber: 123,
                  columnNumber: 27
                }, this)
              ] }, void 0, !0, {
                fileName: "app/components/Navbar/Navbar.tsx",
                lineNumber: 118,
                columnNumber: 25
              }, this)
            ]
          },
          void 0,
          !0,
          {
            fileName: "app/components/Navbar/Navbar.tsx",
            lineNumber: 93,
            columnNumber: 23
          },
          this
        ),
        " "
      ] }, void 0, !0, {
        fileName: "app/components/Navbar/Navbar.tsx",
        lineNumber: 92,
        columnNumber: 21
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("ul", { className: "hidden lg:flex px-4 mr-auto font-semibold font-heading space-x-12", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("li", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
          import_react3.Link,
          {
            to: "/",
            onClick: handleBurgerToggle,
            className: "hover:text-gray-200",
            children: "Home"
          },
          void 0,
          !1,
          {
            fileName: "app/components/Navbar/Navbar.tsx",
            lineNumber: 129,
            columnNumber: 25
          },
          this
        ) }, void 0, !1, {
          fileName: "app/components/Navbar/Navbar.tsx",
          lineNumber: 128,
          columnNumber: 23
        }, this),
        !context2.token && /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("li", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
          import_react3.Link,
          {
            to: "/auth",
            onClick: handleBurgerToggle,
            className: "hover:text-gray-200",
            children: "Login"
          },
          void 0,
          !1,
          {
            fileName: "app/components/Navbar/Navbar.tsx",
            lineNumber: 139,
            columnNumber: 27
          },
          this
        ) }, void 0, !1, {
          fileName: "app/components/Navbar/Navbar.tsx",
          lineNumber: 138,
          columnNumber: 25
        }, this),
        context2.token && /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("li", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
          import_react3.Link,
          {
            to: "/",
            onClick: context2.logout,
            className: "hover:text-gray-200",
            children: "Logout"
          },
          void 0,
          !1,
          {
            fileName: "app/components/Navbar/Navbar.tsx",
            lineNumber: 150,
            columnNumber: 27
          },
          this
        ) }, void 0, !1, {
          fileName: "app/components/Navbar/Navbar.tsx",
          lineNumber: 149,
          columnNumber: 25
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("li", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
          import_react3.Link,
          {
            to: "/products",
            onClick: handleBurgerToggle,
            className: "hover:text-gray-200",
            children: "Products"
          },
          void 0,
          !1,
          {
            fileName: "app/components/Navbar/Navbar.tsx",
            lineNumber: 160,
            columnNumber: 25
          },
          this
        ) }, void 0, !1, {
          fileName: "app/components/Navbar/Navbar.tsx",
          lineNumber: 159,
          columnNumber: 23
        }, this)
      ] }, void 0, !0, {
        fileName: "app/components/Navbar/Navbar.tsx",
        lineNumber: 127,
        columnNumber: 21
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "flex items-center space-x-5 ml-auto", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "relative flex items-center", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
            "svg",
            {
              onClick: handleSearchIconClick,
              className: `w-6 h-6 cursor-pointer ${isSearchOpen ? "absolute left-10" : ""}`,
              viewBox: "0 0 24 24",
              fill: "none",
              xmlns: "http://www.w3.org/2000/svg",
              stroke: "#ffffff",
              children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("g", { id: "SVGRepo_bgCarrier", strokeWidth: "0" }, void 0, !1, {
                  fileName: "app/components/Navbar/Navbar.tsx",
                  lineNumber: 182,
                  columnNumber: 27
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                  "g",
                  {
                    id: "SVGRepo_tracerCarrier",
                    strokeLinecap: "round",
                    strokeLinejoin: "round"
                  },
                  void 0,
                  !1,
                  {
                    fileName: "app/components/Navbar/Navbar.tsx",
                    lineNumber: 183,
                    columnNumber: 27
                  },
                  this
                ),
                /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("g", { id: "SVGRepo_iconCarrier", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                  "path",
                  {
                    clipRule: "evenodd",
                    d: "m3.75 11c0-4.00406 3.24594-7.25 7.25-7.25 4.0041 0 7.25 3.24594 7.25 7.25 0 1.9606-.7782 3.7394-2.0427 5.0444-.0306.0225-.0599.0476-.0876.0753s-.0528.057-.0753.0876c-1.3049 1.2644-3.0838 2.0427-5.0444 2.0427-4.00406 0-7.25-3.2459-7.25-7.25zm12.8842 6.6949c-1.5222 1.2824-3.488 2.0551-5.6342 2.0551-4.83249 0-8.75-3.9175-8.75-8.75 0-4.83249 3.91751-8.75 8.75-8.75 4.8325 0 8.75 3.91751 8.75 8.75 0 2.1462-.7727 4.112-2.0551 5.6342l3.8354 3.8355c.2929.2929.2929.7677 0 1.0606s-.7677.2929-1.0606 0z",
                    fill: "#ffffff",
                    fillRule: "evenodd"
                  },
                  void 0,
                  !1,
                  {
                    fileName: "app/components/Navbar/Navbar.tsx",
                    lineNumber: 189,
                    columnNumber: 29
                  },
                  this
                ) }, void 0, !1, {
                  fileName: "app/components/Navbar/Navbar.tsx",
                  lineNumber: 188,
                  columnNumber: 27
                }, this)
              ]
            },
            void 0,
            !0,
            {
              fileName: "app/components/Navbar/Navbar.tsx",
              lineNumber: 172,
              columnNumber: 25
            },
            this
          ),
          /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("form", { onSubmit: handleFormSubmit, children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
            "input",
            {
              className: `${isSearchOpen ? "flex" : "hidden"} ml-8 p-2 border-white border-solid border-2 rounded-lg bg-rose-900 text-sm`,
              type: "text",
              placeholder: "Search...",
              style: { paddingLeft: "35px" },
              onChange: (e) => setSearchQuery(e.target.value),
              onBlur: () => setIsSearchOpen(!1)
            },
            void 0,
            !1,
            {
              fileName: "app/components/Navbar/Navbar.tsx",
              lineNumber: 198,
              columnNumber: 27
            },
            this
          ) }, void 0, !1, {
            fileName: "app/components/Navbar/Navbar.tsx",
            lineNumber: 197,
            columnNumber: 25
          }, this)
        ] }, void 0, !0, {
          fileName: "app/components/Navbar/Navbar.tsx",
          lineNumber: 171,
          columnNumber: 23
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_react3.Link, { to: "/wishlist", className: "hover:text-gray-200", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
          "svg",
          {
            xmlns: "http://www.w3.org/2000/svg",
            className: "h-6 w-6",
            fill: "none",
            viewBox: "0 0 24 24",
            stroke: "currentColor",
            children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
              "path",
              {
                strokeLinecap: "round",
                strokeLinejoin: "round",
                strokeWidth: "2",
                d: "M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
              },
              void 0,
              !1,
              {
                fileName: "app/components/Navbar/Navbar.tsx",
                lineNumber: 218,
                columnNumber: 27
              },
              this
            )
          },
          void 0,
          !1,
          {
            fileName: "app/components/Navbar/Navbar.tsx",
            lineNumber: 211,
            columnNumber: 25
          },
          this
        ) }, void 0, !1, {
          fileName: "app/components/Navbar/Navbar.tsx",
          lineNumber: 210,
          columnNumber: 23
        }, this),
        context2.token && /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_jsx_dev_runtime2.Fragment, { children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
            import_react3.Link,
            {
              to: "/cart",
              className: "flex items-center hover:text-gray-200",
              children: [
                cartItemsCount,
                /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                  "svg",
                  {
                    xmlns: "http://www.w3.org/2000/svg",
                    className: "h-6 w-6",
                    fill: "none",
                    viewBox: "0 0 24 24",
                    stroke: "currentColor",
                    children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                      "path",
                      {
                        strokeLinecap: "round",
                        strokeLinejoin: "round",
                        strokeWidth: "2",
                        d: "M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
                      },
                      void 0,
                      !1,
                      {
                        fileName: "app/components/Navbar/Navbar.tsx",
                        lineNumber: 240,
                        columnNumber: 31
                      },
                      this
                    )
                  },
                  void 0,
                  !1,
                  {
                    fileName: "app/components/Navbar/Navbar.tsx",
                    lineNumber: 233,
                    columnNumber: 29
                  },
                  this
                ),
                /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("span", { className: "flex absolute -mt-5 ml-4", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("span", { className: "animate-ping absolute inline-flex h-3 w-3 rounded-full bg-pink-400 opacity-75" }, void 0, !1, {
                    fileName: "app/components/Navbar/Navbar.tsx",
                    lineNumber: 248,
                    columnNumber: 31
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("span", { className: "relative inline-flex rounded-full h-3 w-3 bg-pink-500" }, void 0, !1, {
                    fileName: "app/components/Navbar/Navbar.tsx",
                    lineNumber: 249,
                    columnNumber: 31
                  }, this)
                ] }, void 0, !0, {
                  fileName: "app/components/Navbar/Navbar.tsx",
                  lineNumber: 247,
                  columnNumber: 29
                }, this)
              ]
            },
            void 0,
            !0,
            {
              fileName: "app/components/Navbar/Navbar.tsx",
              lineNumber: 228,
              columnNumber: 27
            },
            this
          ),
          /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
            import_react3.Link,
            {
              to: "/user",
              className: "flex items-center hover:text-gray-200",
              children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                "svg",
                {
                  xmlns: "http://www.w3.org/2000/svg",
                  className: "h-6 w-6 hover:text-gray-200",
                  fill: "none",
                  viewBox: "0 0 24 24",
                  stroke: "currentColor",
                  children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                    "path",
                    {
                      strokeLinecap: "round",
                      strokeLinejoin: "round",
                      strokeWidth: "2",
                      d: "M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                    },
                    void 0,
                    !1,
                    {
                      fileName: "app/components/Navbar/Navbar.tsx",
                      lineNumber: 263,
                      columnNumber: 31
                    },
                    this
                  )
                },
                void 0,
                !1,
                {
                  fileName: "app/components/Navbar/Navbar.tsx",
                  lineNumber: 256,
                  columnNumber: 29
                },
                this
              )
            },
            void 0,
            !1,
            {
              fileName: "app/components/Navbar/Navbar.tsx",
              lineNumber: 252,
              columnNumber: 27
            },
            this
          )
        ] }, void 0, !0, {
          fileName: "app/components/Navbar/Navbar.tsx",
          lineNumber: 227,
          columnNumber: 25
        }, this)
      ] }, void 0, !0, {
        fileName: "app/components/Navbar/Navbar.tsx",
        lineNumber: 170,
        columnNumber: 21
      }, this)
    ] }, void 0, !0, {
      fileName: "app/components/Navbar/Navbar.tsx",
      lineNumber: 59,
      columnNumber: 19
    }, this) }, void 0, !1, {
      fileName: "app/components/Navbar/Navbar.tsx",
      lineNumber: 58,
      columnNumber: 17
    }, this) }, void 0, !1, {
      fileName: "app/components/Navbar/Navbar.tsx",
      lineNumber: 57,
      columnNumber: 15
    }, this) }, void 0, !1, {
      fileName: "app/components/Navbar/Navbar.tsx",
      lineNumber: 56,
      columnNumber: 13
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
      "div",
      {
        className: `lg:hidden z-20 absolute top-0 left-0 h-full w-60 transform duration-300 ${isBurgerOpen ? "translate-x-full" : "translate-x-0"}`,
        children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
          "nav",
          {
            id: "sidenav-3",
            className: "bg-rose-900 fixed left-0 top-100 h-screen w-60 -translate-x-full overflow-y-visible white shadow-[0_4px_12px_0_rgba(0,0,0,0.07),_0_2px_4px_rgba(0,0,0,0.05)] data-[te-sidenav-hidden='false']:translate-x-0",
            "data-te-sidenav-init": !0,
            "data-te-sidenav-hidden": "true",
            "data-te-sidenav-color": "white",
            children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
              "ul",
              {
                className: "relative mt-28 list-none px-[0.2rem]",
                "data-te-sidenav-menu-ref": !0,
                children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("li", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                    import_react3.Link,
                    {
                      to: "/",
                      className: "flex h-12 cursor-pointer items-center truncate rounded-[5px] px-6 py-4 text-[0.875rem] text-white font-bold outline-none transition duration-300 ease-linear hover:bg-slate-50 hover:text-inherit hover:outline-none focus:bg-slate-50 focus:text-inherit focus:outline-none active:bg-slate-50 active:text-inherit active:outline-none data-[te-sidenav-state-active]:text-inherit data-[te-sidenav-state-focus]:outline-none motion-reduce:transition-none dark:text-gray-300 dark:hover:bg-white/10 dark:focus:bg-white/10 dark:active:bg-white/10",
                      children: "Home"
                    },
                    void 0,
                    !1,
                    {
                      fileName: "app/components/Navbar/Navbar.tsx",
                      lineNumber: 295,
                      columnNumber: 21
                    },
                    this
                  ) }, void 0, !1, {
                    fileName: "app/components/Navbar/Navbar.tsx",
                    lineNumber: 294,
                    columnNumber: 19
                  }, this),
                  !context2.token && /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("li", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                    import_react3.Link,
                    {
                      to: "/auth",
                      className: "flex h-12 cursor-pointer items-center truncate rounded-[5px] px-6 py-4 text-[0.875rem] text-white font-bold outline-none transition duration-300 ease-linear hover:bg-slate-50 hover:text-inherit hover:outline-none focus:bg-slate-50 focus:text-inherit focus:outline-none active:bg-slate-50 active:text-inherit active:outline-none data-[te-sidenav-state-active]:text-inherit data-[te-sidenav-state-focus]:outline-none motion-reduce:transition-none dark:text-gray-300 dark:hover:bg-white/10 dark:focus:bg-white/10 dark:active:bg-white/10",
                      children: "Login"
                    },
                    void 0,
                    !1,
                    {
                      fileName: "app/components/Navbar/Navbar.tsx",
                      lineNumber: 304,
                      columnNumber: 23
                    },
                    this
                  ) }, void 0, !1, {
                    fileName: "app/components/Navbar/Navbar.tsx",
                    lineNumber: 303,
                    columnNumber: 21
                  }, this),
                  context2.token && /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("li", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                    import_react3.Link,
                    {
                      to: "/",
                      onClick: context2.logout,
                      className: "flex h-12 cursor-pointer items-center truncate rounded-[5px] px-6 py-4 text-[0.875rem] text-white font-bold outline-none transition duration-300 ease-linear hover:bg-slate-50 hover:text-inherit hover:outline-none focus:bg-slate-50 focus:text-inherit focus:outline-none active:bg-slate-50 active:text-inherit active:outline-none data-[te-sidenav-state-active]:text-inherit data-[te-sidenav-state-focus]:outline-none motion-reduce:transition-none dark:text-gray-300 dark:hover:bg-white/10 dark:focus:bg-white/10 dark:active:bg-white/10",
                      children: "Logout"
                    },
                    void 0,
                    !1,
                    {
                      fileName: "app/components/Navbar/Navbar.tsx",
                      lineNumber: 314,
                      columnNumber: 23
                    },
                    this
                  ) }, void 0, !1, {
                    fileName: "app/components/Navbar/Navbar.tsx",
                    lineNumber: 313,
                    columnNumber: 21
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("li", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(
                    import_react3.Link,
                    {
                      to: "/products",
                      className: "flex h-12 cursor-pointer items-center truncate rounded-[5px] px-6 py-4 text-[0.875rem] text-white font-bold outline-none transition duration-300 ease-linear hover:bg-slate-50 hover:text-inherit hover:outline-none focus:bg-slate-50 focus:text-inherit focus:outline-none active:bg-slate-50 active:text-inherit active:outline-none data-[te-sidenav-state-active]:text-inherit data-[te-sidenav-state-focus]:outline-none motion-reduce:transition-none dark:text-gray-300 dark:hover:bg-white/10 dark:focus:bg-white/10 dark:active:bg-white/10",
                      children: "Products"
                    },
                    void 0,
                    !1,
                    {
                      fileName: "app/components/Navbar/Navbar.tsx",
                      lineNumber: 325,
                      columnNumber: 21
                    },
                    this
                  ) }, void 0, !1, {
                    fileName: "app/components/Navbar/Navbar.tsx",
                    lineNumber: 324,
                    columnNumber: 19
                  }, this)
                ]
              },
              void 0,
              !0,
              {
                fileName: "app/components/Navbar/Navbar.tsx",
                lineNumber: 290,
                columnNumber: 17
              },
              this
            )
          },
          void 0,
          !1,
          {
            fileName: "app/components/Navbar/Navbar.tsx",
            lineNumber: 283,
            columnNumber: 15
          },
          this
        )
      },
      void 0,
      !1,
      {
        fileName: "app/components/Navbar/Navbar.tsx",
        lineNumber: 278,
        columnNumber: 13
      },
      this
    )
  ] }, void 0, !0, {
    fileName: "app/components/Navbar/Navbar.tsx",
    lineNumber: 55,
    columnNumber: 11
  }, this) }, void 0, !1, {
    fileName: "app/components/Navbar/Navbar.tsx",
    lineNumber: 52,
    columnNumber: 5
  }, this);
}, Navbar_default = Navbar;

// app/styles/tailwind.css
var tailwind_default = "/build/_assets/tailwind-NM7JICOO.css";

// app/root.tsx
var import_react6 = require("react");
var import_client4 = require("@apollo/client"), import_jsx_dev_runtime3 = require("react/jsx-dev-runtime"), links = () => [{ rel: "stylesheet", href: tailwind_default }], meta = () => ({
  charset: "utf-8",
  title: "timix",
  viewport: "width=device-width,initial-scale=1"
});
function App() {
  let context = (0, import_react6.useContext)(auth_context_default), [token, setToken] = (0, import_react6.useState)(null), [userId, setUserId] = (0, import_react6.useState)(null), [role, setRole] = (0, import_react6.useState)(null), client = (0, import_client4.useApolloClient)();
  (0, import_react6.useEffect)(() => {
    async function fetchData() {
      let data = await refreshToken();
      data && data.ok && login(data.accessToken, data.userId, null, data.role);
    }
    fetchData();
  }, []);
  let login = (token2, userId2, tokenExpiration, role2) => {
    setToken(token2), setUserId(userId2), setRole(role2);
  }, logout = () => {
    setToken(null), setUserId(null);
  };
  return /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("html", { lang: "en", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("head", { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)(import_react5.Meta, {}, void 0, !1, {
        fileName: "app/root.tsx",
        lineNumber: 57,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)(import_react5.Links, {}, void 0, !1, {
        fileName: "app/root.tsx",
        lineNumber: 58,
        columnNumber: 9
      }, this)
    ] }, void 0, !0, {
      fileName: "app/root.tsx",
      lineNumber: 56,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("body", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)(
      auth_context_default.Provider,
      {
        value: {
          token,
          userId,
          login,
          logout,
          role
        },
        children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)(Navbar_default, {}, void 0, !1, {
            fileName: "app/root.tsx",
            lineNumber: 70,
            columnNumber: 11
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)(import_react5.Outlet, {}, void 0, !1, {
            fileName: "app/root.tsx",
            lineNumber: 71,
            columnNumber: 11
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)(import_react5.ScrollRestoration, {}, void 0, !1, {
            fileName: "app/root.tsx",
            lineNumber: 72,
            columnNumber: 11
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)(import_react5.Scripts, {}, void 0, !1, {
            fileName: "app/root.tsx",
            lineNumber: 73,
            columnNumber: 11
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)(import_react5.LiveReload, {}, void 0, !1, {
            fileName: "app/root.tsx",
            lineNumber: 74,
            columnNumber: 11
          }, this)
        ]
      },
      void 0,
      !0,
      {
        fileName: "app/root.tsx",
        lineNumber: 61,
        columnNumber: 9
      },
      this
    ) }, void 0, !1, {
      fileName: "app/root.tsx",
      lineNumber: 60,
      columnNumber: 7
    }, this)
  ] }, void 0, !0, {
    fileName: "app/root.tsx",
    lineNumber: 55,
    columnNumber: 5
  }, this);
}

// app/routes/products/index.tsx
var products_exports = {};
__export(products_exports, {
  action: () => action,
  default: () => products_default
});
var import_client7 = require("@apollo/client"), import_react9 = require("@remix-run/react"), import_react10 = require("react");

// app/components/Spinner/Spinner.js
var import_jsx_dev_runtime4 = require("react/jsx-dev-runtime"), Spinner = () => /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "flex justify-center items-center", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { role: "status", children: [
  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
    "svg",
    {
      "aria-hidden": "true",
      className: "inline w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-rose-900",
      viewBox: "0 0 100 101",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg",
      children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
          "path",
          {
            d: "M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z",
            fill: "currentColor"
          },
          void 0,
          !1,
          {
            fileName: "app/components/Spinner/Spinner.js",
            lineNumber: 11,
            columnNumber: 9
          },
          this
        ),
        /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
          "path",
          {
            d: "M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z",
            fill: "currentFill"
          },
          void 0,
          !1,
          {
            fileName: "app/components/Spinner/Spinner.js",
            lineNumber: 15,
            columnNumber: 9
          },
          this
        )
      ]
    },
    void 0,
    !0,
    {
      fileName: "app/components/Spinner/Spinner.js",
      lineNumber: 4,
      columnNumber: 7
    },
    this
  ),
  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { className: "sr-only", children: "Loading..." }, void 0, !1, {
    fileName: "app/components/Spinner/Spinner.js",
    lineNumber: 20,
    columnNumber: 7
  }, this)
] }, void 0, !0, {
  fileName: "app/components/Spinner/Spinner.js",
  lineNumber: 3,
  columnNumber: 5
}, this) }, void 0, !1, {
  fileName: "app/components/Spinner/Spinner.js",
  lineNumber: 2,
  columnNumber: 3
}, this), Spinner_default = Spinner;

// app/components/Product/Product.tsx
var import_client5 = require("@apollo/client");

// app/cache/ProductsCache.ts
var updateProductsCache = (cache, productId, isProductInWishlist, userId) => {
  try {
    let products = cache.readQuery({
      query: GET_PRODUCTS,
      variables: {
        userId
      }
    });
    if (!products || !products.products)
      return;
    let index = products.products.findIndex(
      (product) => product._id === productId
    );
    if (index === -1)
      return;
    let updatedProducts = [...products.products];
    updatedProducts[index].isProductInWishlist = isProductInWishlist, cache.writeQuery({
      query: GET_PRODUCTS,
      variables: {
        userId
      },
      data: {
        products: updatedProducts
      }
    });
  } catch (error) {
    console.error("Error in updateProductsCache:", error);
  }
}, updateAnonymousUserProductsCache = (cache, productId, isProductInWishlist) => {
  try {
    let products = cache.readQuery({
      query: GET_PRODUCTS,
      variables: {
        userId: null
      }
    });
    if (console.log(123, products), !products || !products.products)
      return;
    let index = products.products.findIndex(
      (product) => product._id === productId
    );
    if (index === -1)
      return;
    let updatedProducts = products.products.map(
      (product, i) => i === index ? { ...product, isProductInWishlist } : product
    );
    cache.writeQuery({
      query: GET_PRODUCTS,
      variables: {
        userId: null
      },
      data: {
        products: updatedProducts
      }
    });
  } catch (error) {
    console.error("Error in updateAnonymousUserProductsCache:", error);
  }
}, anonymousUserWishlistCache = (cache, productId, productInWishlist) => {
  let product = cache.readQuery({
    query: GET_PRODUCT,
    variables: { productId, userId: null }
  });
  if (product) {
    let updatedProduct = {
      ...product.product,
      isProductInWishlist: productInWishlist
    };
    cache.writeQuery({
      query: GET_PRODUCT,
      variables: { productId, userId: null },
      data: {
        product: updatedProduct
      }
    });
  }
}, deleteCartItemFromProductsCache = (cache, productId, isProductInCart, userId) => {
  try {
    let products = cache.readQuery({
      query: GET_PRODUCTS,
      variables: {
        userId
      }
    });
    if (!products || !products.products)
      return;
    let index = products.products.findIndex(
      (product) => product._id === productId
    );
    if (index === -1)
      return;
    let updatedProducts = [...products.products];
    updatedProducts[index].isProductInCart = isProductInCart, cache.writeQuery({
      query: GET_PRODUCTS,
      variables: {
        userId
      },
      data: {
        products: updatedProducts
      }
    });
  } catch (error) {
    console.error("Error in deleteCartItemFromProductsCache:", error);
  }
}, updateCartItemQuantityCache = (cache, productId, quantity, userId) => {
  try {
    let cartItems = cache.readQuery({
      query: GET_USER_CART_PRODUCTS,
      variables: {
        userId
      }
    });
    if (console.log(123, cartItems), !cartItems || !cartItems.userCartItems)
      return;
    let index = cartItems.userCartItems.findIndex(
      (item) => item.product._id === productId
    );
    if (index === -1)
      return;
    let updatedCartItems = [...cartItems.userCartItems], updatedItem = { ...updatedCartItems[index], quantity };
    updatedCartItems[index] = updatedItem, cache.writeQuery({
      query: GET_USER_CART_PRODUCTS,
      variables: {
        userId
      },
      data: {
        userCartItems: updatedCartItems
      }
    });
  } catch (error) {
    console.error("Error in updateCartItemQuantityCache:", error);
  }
}, deleteItemFromCartCache = (cache, productId, userId) => {
  try {
    let cartItems = cache.readQuery({
      query: GET_USER_CART_PRODUCTS,
      variables: {
        userId
      }
    });
    if (!cartItems || !cartItems.userCartItems)
      return;
    let updatedCartItems = cartItems.userCartItems.filter(
      (item) => item.product._id !== productId
    );
    cache.writeQuery({
      query: GET_USER_CART_PRODUCTS,
      variables: {
        userId
      },
      data: {
        userCartItems: updatedCartItems
      }
    });
  } catch (error) {
    console.error("Error in deleteItemFromCartCache:", error);
  }
}, addItemToCartCache = (cache, cartItem, userId) => {
  try {
    let cartItems = cache.readQuery({
      query: GET_USER_CART_PRODUCTS,
      variables: {
        userId
      }
    });
    if (!cartItems || !cartItems.userCartItems)
      return;
    let existingCartItemIndex = cartItems.userCartItems.findIndex(
      (item) => item.product._id === cartItem.product._id
    );
    if (existingCartItemIndex !== -1) {
      if (cartItem.quantity > 1) {
        let updatedCartItems = cartItems.userCartItems.map(
          (item, index) => index === existingCartItemIndex ? { ...item, quantity: cartItem.quantity } : item
        );
        cache.writeQuery({
          query: GET_USER_CART_PRODUCTS,
          variables: {
            userId
          },
          data: {
            userCartItems: updatedCartItems
          }
        });
      }
    } else {
      let updatedCartItems = [...cartItems.userCartItems, cartItem];
      cache.writeQuery({
        query: GET_USER_CART_PRODUCTS,
        variables: {
          userId
        },
        data: {
          userCartItems: updatedCartItems
        }
      });
    }
  } catch (error) {
    console.error("Error in addItemToCartCache:", error);
  }
}, deleteItemFromWishlistCache = (cache, productId, userId) => {
  try {
    let existingData = cache.readQuery({
      query: GET_USER_WISHLIST,
      variables: {
        userId
      }
    });
    if (!existingData || !existingData.userWishlist)
      return;
    let updatedWishlistItems = existingData.userWishlist.filter(
      (item) => item._id !== productId
    );
    cache.writeQuery({
      query: GET_USER_WISHLIST,
      variables: {
        userId
      },
      data: {
        userWishlist: updatedWishlistItems
      }
    });
  } catch (error) {
    console.error("Error in deleteItemFromWishlistCache:", error);
  }
}, addItemToWishlistCache = (cache, wishlistItem, userId) => {
  try {
    let existingData = cache.readQuery({
      query: GET_USER_WISHLIST,
      variables: {
        userId
      }
    });
    if (!existingData || !existingData.userWishlist)
      return;
    let updatedWishlistItems = [...existingData.userWishlist, wishlistItem];
    cache.writeQuery({
      query: GET_USER_WISHLIST,
      variables: {
        userId
      },
      data: {
        userWishlist: updatedWishlistItems
      }
    });
  } catch (error) {
    console.error("Error in addItemToWishlistCache:", error);
  }
}, deleteProductCache = (cache, productId, userId) => {
  try {
    let existingData = cache.readQuery({
      query: GET_PRODUCTS,
      variables: {
        userId
      }
    });
    if (!existingData)
      return;
    let updatedProducts = filterOutProductById(
      existingData.products,
      productId
    );
    cache.writeQuery({
      query: GET_PRODUCTS,
      variables: {
        userId
      },
      data: {
        products: updatedProducts
      }
    });
  } catch (error) {
    console.error("Error in deleteProductCache:", error);
  }
}, addProductCache = (cache, product, userId) => {
  try {
    let existingData = cache.readQuery({
      query: GET_PRODUCTS,
      variables: {
        userId
      }
    });
    if (console.log("existingData", existingData), !existingData) {
      let newData = { products: [product] };
      cache.writeQuery({
        query: GET_PRODUCTS,
        variables: {
          userId
        },
        data: newData
      }), console.log("addProductCache", newData);
      return;
    }
    let updatedProducts = [...existingData.products, product];
    cache.writeQuery({
      query: GET_PRODUCTS,
      variables: {
        userId
      },
      data: {
        products: updatedProducts
      }
    });
  } catch (error) {
    console.error("Error in addProductCache:", error);
  }
}, filterOutProductById = (products, productId) => products.filter((item) => item._id !== productId);

// app/components/Product/Product.tsx
var import_client6 = require("@apollo/client");

// app/utils/utils.ts
async function validationAction({
  request,
  schema: schema3
}) {
  let body = Object.fromEntries(await request.formData());
  try {
    return { formData: schema3.parse(body), errors: null };
  } catch (err) {
    return {
      formData: body,
      errors: err.issues.reduce((acc, curr) => {
        let key = curr.path[0];
        return acc[key] = curr.message, acc;
      }, {})
    };
  }
}
var getTempWishlistFromBrowser = () => {
  if (typeof localStorage < "u") {
    let tempWishlistStr = localStorage.getItem("tempWishlist");
    if (tempWishlistStr)
      return JSON.parse(tempWishlistStr);
  }
  return null;
}, saveTempWishlistToBrowser = (tempWishlist) => {
  localStorage.setItem("tempWishlist", JSON.stringify(tempWishlist));
};

// app/components/Product/Product.tsx
var import_react7 = require("react"), import_jsx_dev_runtime5 = require("react/jsx-dev-runtime"), Product = ({
  product,
  userId
}) => {
  let client = (0, import_client6.useApolloClient)(), context = (0, import_react7.useContext)(auth_context_default), addProductToWishlistMutation = (0, import_client5.useMutation)(ADD_PRODUCT_TO_WISHLIST)[0], removeProductFromWishlistMutation = (0, import_client5.useMutation)(
    REMOVE_PRODUCT_FROM_WISHLIST
  )[0], deleteProductMutation = (0, import_client5.useMutation)(DELETE_PRODUCT)[0], addProductToCartMutation = (0, import_client5.useMutation)(ADD_PRODUCT_TO_CART)[0], wishlistButtonHandler = () => {
    product.isProductInWishlist ? removeProductFromWishlist(product._id, userId) : addProductToWishlist(product._id, userId);
  }, addProductToWishlist = async (productId, userId2) => {
    if (userId2) {
      let res = await addProductToWishlistMutation({
        variables: { productId, userId: userId2 }
      }), { addToWishlist } = res.data;
      addToWishlist && (updateProductsCache(client.cache, productId, !0, userId2), addItemToWishlistCache(client.cache, addToWishlist, userId2));
    } else {
      let tempWishlist = getTempWishlistFromBrowser();
      tempWishlist || (tempWishlist = { products: [] }), tempWishlist.products.push(productId), saveTempWishlistToBrowser(tempWishlist), updateAnonymousUserProductsCache(client.cache, productId, !0);
    }
  }, removeProductFromWishlist = async (productId, userId2) => {
    if (userId2) {
      let res = await removeProductFromWishlistMutation({
        variables: { productId, userId: userId2 }
      }), { removeFromWishlist } = res.data;
      removeFromWishlist && (updateProductsCache(client.cache, productId, !1, userId2), deleteItemFromWishlistCache(client.cache, productId, userId2));
    } else {
      let tempWishlist = getTempWishlistFromBrowser();
      if (tempWishlist) {
        updateAnonymousUserProductsCache(client.cache, productId, !1);
        let index = tempWishlist.products.indexOf(productId);
        index !== -1 && tempWishlist.products.splice(index, 1);
      }
      saveTempWishlistToBrowser(tempWishlist);
    }
  }, deleteProductHandler = async () => {
    try {
      await deleteProductMutation({
        variables: { productId: product._id, userId }
      }), deleteProductCache(client.cache, product._id, userId);
    } catch (err) {
      console.error(err);
    }
  }, addProductToCart = async () => {
    let cartItem = (await addProductToCartMutation({
      variables: { productId: product._id, userId }
    })).data.addToCart;
    cartItem && (product.isProductInCart ? updateCartItemQuantityCache(
      client.cache,
      product._id,
      cartItem.quantity,
      userId
    ) : addItemToCartCache(client.cache, cartItem, userId));
  };
  return /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("div", { className: "bg-white p-3 shadow-lg relative", children: [
    context.role === "ADMIN" && /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
      "svg",
      {
        xmlns: "http://www.w3.org/2000/svg",
        viewBox: "0 0 24 24",
        className: "h-5 w-5 ml-auto",
        onClick: (event) => {
          event.preventDefault(), event.stopPropagation(), deleteProductHandler();
        },
        children: /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
          "path",
          {
            fill: "currentColor",
            d: "M17.6 6.6l-4.6 4.6 4.6 4.6-1.4 1.4-4.6-4.6-4.6 4.6-1.4-1.4 4.6-4.6-4.6-4.6 1.4-1.4 4.6 4.6 4.6-4.6z"
          },
          void 0,
          !1,
          {
            fileName: "app/components/Product/Product.tsx",
            lineNumber: 139,
            columnNumber: 11
          },
          this
        )
      },
      void 0,
      !1,
      {
        fileName: "app/components/Product/Product.tsx",
        lineNumber: 129,
        columnNumber: 9
      },
      this
    ),
    /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(import_jsx_dev_runtime5.Fragment, { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("div", { className: "relative flex items-end overflow-hidden z-10", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
          "img",
          {
            id: "image",
            className: "w-full h-full object-cover",
            src: `https://source.unsplash.com/800x600/?${product.category ? product.category : "wallet"}`
          },
          void 0,
          !1,
          {
            fileName: "app/components/Product/Product.tsx",
            lineNumber: 147,
            columnNumber: 11
          },
          this
        ),
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
          "div",
          {
            className: "absolute top-3 right-3",
            onClick: (event) => {
              event.preventDefault(), event.stopPropagation(), wishlistButtonHandler();
            },
            children: /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
              "svg",
              {
                xmlns: "http://www.w3.org/2000/svg",
                className: `h-6 w-6 ${product != null && product.isProductInWishlist ? "text-red-600" : "text-white hover:text-red-200"}`,
                viewBox: "0 0 20 20",
                fill: "currentColor",
                children: /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
                  "path",
                  {
                    fillRule: "evenodd",
                    d: "M10 17.297l-1.635-1.488C3.704 11.05 2 8.665 2 6.412c0-2.483 2.017-4.5 4.5-4.5 1.614 0 3.061.85 3.875 2.122C11.439 2.762 12.886 2 14.5 2c2.483 0 4.5 2.017 4.5 4.5 0 2.253-1.704 4.638-6.365 9.397L10 17.297z",
                    clipRule: "evenodd"
                  },
                  void 0,
                  !1,
                  {
                    fileName: "app/components/Product/Product.tsx",
                    lineNumber: 172,
                    columnNumber: 15
                  },
                  this
                )
              },
              void 0,
              !1,
              {
                fileName: "app/components/Product/Product.tsx",
                lineNumber: 162,
                columnNumber: 13
              },
              this
            )
          },
          void 0,
          !1,
          {
            fileName: "app/components/Product/Product.tsx",
            lineNumber: 154,
            columnNumber: 11
          },
          this
        ),
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("div", { className: "absolute bottom-3 left-3 inline-flex items-center rounded-lg bg-white p-2 shadow-md", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
            "svg",
            {
              xmlns: "http://www.w3.org/2000/svg",
              className: "h-5 w-5 text-yellow-400",
              viewBox: "0 0 20 20",
              fill: "currentColor",
              children: /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("path", { d: "M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" }, void 0, !1, {
                fileName: "app/components/Product/Product.tsx",
                lineNumber: 186,
                columnNumber: 15
              }, this)
            },
            void 0,
            !1,
            {
              fileName: "app/components/Product/Product.tsx",
              lineNumber: 180,
              columnNumber: 13
            },
            this
          ),
          /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("span", { className: "ml-1 text-sm text-slate-400", children: "4.9" }, void 0, !1, {
            fileName: "app/components/Product/Product.tsx",
            lineNumber: 188,
            columnNumber: 13
          }, this)
        ] }, void 0, !0, {
          fileName: "app/components/Product/Product.tsx",
          lineNumber: 179,
          columnNumber: 11
        }, this)
      ] }, void 0, !0, {
        fileName: "app/components/Product/Product.tsx",
        lineNumber: 146,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("div", { className: "mt-1 p-2", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("h2", { className: "text-slate-700", children: product.name }, void 0, !1, {
          fileName: "app/components/Product/Product.tsx",
          lineNumber: 192,
          columnNumber: 11
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("p", { className: "mt-1 text-sm text-slate-400", children: product.description }, void 0, !1, {
          fileName: "app/components/Product/Product.tsx",
          lineNumber: 193,
          columnNumber: 11
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("div", { className: "mt-3 flex items-end justify-between", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("p", { className: "text-lg font-bold text-blue-500", children: [
            "$",
            product.price
          ] }, void 0, !0, {
            fileName: "app/components/Product/Product.tsx",
            lineNumber: 196,
            columnNumber: 13
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)("div", { className: "flex items-center space-x-1.5 rounded-lg bg-blue-500 px-4 py-1.5 text-white duration-100 hover:bg-blue-600", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
              "svg",
              {
                xmlns: "http://www.w3.org/2000/svg",
                fill: "none",
                viewBox: "0 0 24 24",
                strokeWidth: "1.5",
                stroke: "currentColor",
                className: "h-4 w-4",
                children: /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
                  "path",
                  {
                    strokeLinecap: "round",
                    strokeLinejoin: "round",
                    d: "M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"
                  },
                  void 0,
                  !1,
                  {
                    fileName: "app/components/Product/Product.tsx",
                    lineNumber: 207,
                    columnNumber: 17
                  },
                  this
                )
              },
              void 0,
              !1,
              {
                fileName: "app/components/Product/Product.tsx",
                lineNumber: 199,
                columnNumber: 15
              },
              this
            ),
            /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
              "button",
              {
                className: "text-sm",
                onClick: (event) => {
                  event.preventDefault(), event.stopPropagation(), addProductToCart();
                },
                children: "Add to cart"
              },
              void 0,
              !1,
              {
                fileName: "app/components/Product/Product.tsx",
                lineNumber: 214,
                columnNumber: 15
              },
              this
            )
          ] }, void 0, !0, {
            fileName: "app/components/Product/Product.tsx",
            lineNumber: 198,
            columnNumber: 13
          }, this)
        ] }, void 0, !0, {
          fileName: "app/components/Product/Product.tsx",
          lineNumber: 195,
          columnNumber: 11
        }, this)
      ] }, void 0, !0, {
        fileName: "app/components/Product/Product.tsx",
        lineNumber: 191,
        columnNumber: 9
      }, this)
    ] }, void 0, !0, {
      fileName: "app/components/Product/Product.tsx",
      lineNumber: 145,
      columnNumber: 7
    }, this)
  ] }, void 0, !0, {
    fileName: "app/components/Product/Product.tsx",
    lineNumber: 127,
    columnNumber: 5
  }, this);
}, Product_default = Product;

// app/utils/constants.ts
var ROLE_ADMIN = "ADMIN";

// app/components/Backdrop/Backdrop.jsx
var import_jsx_dev_runtime6 = require("react/jsx-dev-runtime"), Backdrop = () => /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "fixed z-20 top-0 left-0 h-screen w-screen bg-black bg-opacity-75" }, void 0, !1, {
  fileName: "app/components/Backdrop/Backdrop.jsx",
  lineNumber: 3,
  columnNumber: 5
}, this), Backdrop_default = Backdrop;

// app/components/Modal/Modal.jsx
var import_react8 = require("react"), import_jsx_dev_runtime7 = require("react/jsx-dev-runtime"), Modal = (props) => /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "fixed z-20 top-24 w-3/5 bg-white shadow-md left-0 right-0 mx-auto", children: [
  /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "p-1 bg-rose-900 text-white", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("h1", { className: "m-0 text-lg", children: props.title }, void 0, !1, {
    fileName: "app/components/Modal/Modal.jsx",
    lineNumber: 7,
    columnNumber: 9
  }, this) }, void 0, !1, {
    fileName: "app/components/Modal/Modal.jsx",
    lineNumber: 6,
    columnNumber: 7
  }, this),
  /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)("div", { className: "p-1", children: props.children }, void 0, !1, {
    fileName: "app/components/Modal/Modal.jsx",
    lineNumber: 9,
    columnNumber: 7
  }, this)
] }, void 0, !0, {
  fileName: "app/components/Modal/Modal.jsx",
  lineNumber: 5,
  columnNumber: 5
}, this), Modal_default = Modal;

// app/routes/products/index.tsx
var Z = __toESM(require("zod"));
var import_node = require("@remix-run/node");

// app/utils/toasterNotifications.ts
var import_react_hot_toast = __toESM(require("react-hot-toast")), successToaster = (message) => import_react_hot_toast.default.success(message), errorToaster = (message) => import_react_hot_toast.default.error(message);

// app/routes/products/index.tsx
var import_jsx_dev_runtime8 = require("react/jsx-dev-runtime"), schema = Z.object({
  name: Z.string().min(1, "Name is required"),
  description: Z.string().min(1, "Description is required"),
  price: Z.string().min(1, "Price is required"),
  status: Z.string().min(1, "Status is required"),
  category: Z.string().min(1, "Category is required"),
  images: Z.string().min(1, "Image is required")
}), action = async ({ request }) => {
  let { formData, errors } = await validationAction({
    request,
    schema
  });
  return errors ? (0, import_node.json)({ errors }, { status: 400 }) : formData;
}, Products = () => {
  var _a, _b, _c, _d, _e, _f, _g, _h, _i, _j, _k, _l, _m, _n, _o, _p, _q, _r, _s, _t, _u;
  let addProduct = (0, import_client7.useMutation)(ADD_PRODUCT)[0], [createProduct, setCreateProduct] = (0, import_react10.useState)(!1), context = (0, import_react10.useContext)(auth_context_default), client = (0, import_client7.useApolloClient)(), actionData = (0, import_react9.useActionData)(), userId = context == null ? void 0 : context.userId, [anonymousUserProductsData, setAnonymousUserProductsData] = (0, import_react10.useState)(null), {
    loading: productsLoading,
    error: errorProducts,
    data: productsData
  } = (0, import_client7.useQuery)(GET_PRODUCTS, {
    variables: {
      userId: userId || null
    }
  });
  (0, import_react10.useEffect)(() => {
    if (!userId) {
      let updatedProducts = productsData == null ? void 0 : productsData.products.map(
        (product) => {
          let tempWishlist = getTempWishlistFromBrowser();
          return tempWishlist && tempWishlist.products.includes(product._id) ? { ...product, isProductInWishlist: !0 } : { ...product, isProductInWishlist: !1 };
        }
      );
      setAnonymousUserProductsData(updatedProducts), console.log(123123123, anonymousUserProductsData);
    }
    return () => {
      setAnonymousUserProductsData(null);
    };
  }, [productsData]);
  let createProductHandler = () => {
    setCreateProduct(!0);
  }, modalCancelHandler = () => {
    setCreateProduct(!1);
  }, modalConfirmHandler = () => {
    setCreateProduct(!1);
  }, addProductHandler = async (productInput) => {
    try {
      let product = await addProduct({
        variables: { productInput }
      });
      setCreateProduct(!1), addProductCache(client.cache, product, userId), successToaster("Product created!");
    } catch (err) {
      console.error(err);
    }
  };
  if ((0, import_react10.useEffect)(() => {
    if (actionData && !(actionData != null && actionData.errors)) {
      let productInput = {
        ...actionData,
        userId
      };
      addProductHandler(productInput);
    }
  }, [actionData]), productsLoading)
    return /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(Spinner_default, {}, void 0, !1, {
      fileName: "app/routes/products/index.tsx",
      lineNumber: 124,
      columnNumber: 31
    }, this);
  if (errorProducts)
    return /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("p", { children: "Something went wrong" }, void 0, !1, {
      fileName: "app/routes/products/index.tsx",
      lineNumber: 125,
      columnNumber: 29
    }, this);
  let productsToMap = userId ? productsData == null ? void 0 : productsData.products : anonymousUserProductsData;
  return console.log("productsToMap", productsToMap), /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(import_jsx_dev_runtime8.Fragment, { children: [
    (context == null ? void 0 : context.role) === ROLE_ADMIN && /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "relative", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "w-36 ml-auto mt-2 mr-2 mb-2 flex items-center justify-center", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
        "div",
        {
          className: "flex items-center justify-center w-12 h-12 rounded-full bg-rose-900 text-black cursor-pointer",
          onClick: createProductHandler,
          children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
            "svg",
            {
              xmlns: "http://www.w3.org/2000/svg",
              className: "w-6 h-6",
              fill: "none",
              viewBox: "0 0 24 24",
              stroke: "currentColor",
              children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
                "path",
                {
                  strokeLinecap: "round",
                  strokeLinejoin: "round",
                  strokeWidth: "2",
                  d: "M12 4v16m8-8H4"
                },
                void 0,
                !1,
                {
                  fileName: "app/routes/products/index.tsx",
                  lineNumber: 146,
                  columnNumber: 17
                },
                this
              )
            },
            void 0,
            !1,
            {
              fileName: "app/routes/products/index.tsx",
              lineNumber: 139,
              columnNumber: 15
            },
            this
          )
        },
        void 0,
        !1,
        {
          fileName: "app/routes/products/index.tsx",
          lineNumber: 135,
          columnNumber: 13
        },
        this
      ),
      /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("span", { className: "ml-2 whitespace-nowrap", children: "Add Product" }, void 0, !1, {
        fileName: "app/routes/products/index.tsx",
        lineNumber: 154,
        columnNumber: 13
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/products/index.tsx",
      lineNumber: 134,
      columnNumber: 11
    }, this) }, void 0, !1, {
      fileName: "app/routes/products/index.tsx",
      lineNumber: 133,
      columnNumber: 9
    }, this),
    createProduct && /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(Backdrop_default, {}, void 0, !1, {
      fileName: "app/routes/products/index.tsx",
      lineNumber: 158,
      columnNumber: 25
    }, this),
    createProduct && /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
      Modal_default,
      {
        title: "Add Product",
        onCancel: modalCancelHandler,
        onConfirm: modalConfirmHandler,
        children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(import_react9.Form, { className: "max-w-xl mx-auto", method: "post", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "grid grid-cols-2 gap-4", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "form-control", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
                  "label",
                  {
                    htmlFor: "name",
                    className: "block text-gray-700 font-bold mb-2",
                    children: "Name"
                  },
                  void 0,
                  !1,
                  {
                    fileName: "app/routes/products/index.tsx",
                    lineNumber: 168,
                    columnNumber: 17
                  },
                  this
                ),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
                  "input",
                  {
                    id: "name",
                    type: "text",
                    autoFocus: !0,
                    name: "name",
                    autoComplete: "name",
                    "aria-invalid": (_a = actionData == null ? void 0 : actionData.errors) != null && _a.name ? !0 : void 0,
                    "aria-describedby": "name-error",
                    className: `w-full bg-gray-100 border ${(_b = actionData == null ? void 0 : actionData.errors) != null && _b.name ? "border-red-500" : "border-gray-300"} p-2 rounded-lg focus:outline-none focus:bg-white`
                  },
                  void 0,
                  !1,
                  {
                    fileName: "app/routes/products/index.tsx",
                    lineNumber: 174,
                    columnNumber: 17
                  },
                  this
                ),
                ((_c = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _c.name) && /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("p", { className: "text-red-500 text-sm mt-1", children: (_d = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _d.name }, void 0, !1, {
                  fileName: "app/routes/products/index.tsx",
                  lineNumber: 190,
                  columnNumber: 19
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/products/index.tsx",
                lineNumber: 167,
                columnNumber: 15
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "form-control", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
                  "label",
                  {
                    htmlFor: "category",
                    className: "block text-gray-700 font-bold mb-2",
                    children: "Category"
                  },
                  void 0,
                  !1,
                  {
                    fileName: "app/routes/products/index.tsx",
                    lineNumber: 196,
                    columnNumber: 17
                  },
                  this
                ),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
                  "select",
                  {
                    id: "category",
                    autoFocus: !0,
                    name: "category",
                    autoComplete: "category",
                    "aria-invalid": (_e = actionData == null ? void 0 : actionData.errors) != null && _e.category ? !0 : void 0,
                    "aria-describedby": "category-error",
                    className: `w-full bg-gray-100 border ${(_f = actionData == null ? void 0 : actionData.errors) != null && _f.category ? "border-red-500" : "border-gray-300"} p-2 rounded-lg focus:outline-none focus:bg-white`,
                    children: [
                      /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("option", { value: "", children: "-- Select category --" }, void 0, !1, {
                        fileName: "app/routes/products/index.tsx",
                        lineNumber: 216,
                        columnNumber: 19
                      }, this),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("option", { value: "WALLET", children: "Wallet" }, void 0, !1, {
                        fileName: "app/routes/products/index.tsx",
                        lineNumber: 217,
                        columnNumber: 19
                      }, this),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("option", { value: "BELT", children: "Belt" }, void 0, !1, {
                        fileName: "app/routes/products/index.tsx",
                        lineNumber: 218,
                        columnNumber: 19
                      }, this),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("option", { value: "BRACELET", children: "Bracelet" }, void 0, !1, {
                        fileName: "app/routes/products/index.tsx",
                        lineNumber: 219,
                        columnNumber: 19
                      }, this),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("option", { value: "GLASSES", children: "Glasses" }, void 0, !1, {
                        fileName: "app/routes/products/index.tsx",
                        lineNumber: 220,
                        columnNumber: 19
                      }, this)
                    ]
                  },
                  void 0,
                  !0,
                  {
                    fileName: "app/routes/products/index.tsx",
                    lineNumber: 202,
                    columnNumber: 17
                  },
                  this
                ),
                ((_g = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _g.category) && /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("p", { className: "text-red-500 text-sm mt-1", children: (_h = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _h.category }, void 0, !1, {
                  fileName: "app/routes/products/index.tsx",
                  lineNumber: 223,
                  columnNumber: 19
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/products/index.tsx",
                lineNumber: 195,
                columnNumber: 15
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/products/index.tsx",
              lineNumber: 166,
              columnNumber: 13
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "form-control", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
                "label",
                {
                  htmlFor: "description",
                  className: "block text-gray-700 font-bold mb-2",
                  children: "Description"
                },
                void 0,
                !1,
                {
                  fileName: "app/routes/products/index.tsx",
                  lineNumber: 230,
                  columnNumber: 15
                },
                this
              ),
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
                "textarea",
                {
                  id: "description",
                  autoFocus: !0,
                  name: "description",
                  autoComplete: "description",
                  "aria-invalid": (_i = actionData == null ? void 0 : actionData.errors) != null && _i.description ? !0 : void 0,
                  "aria-describedby": "description-error",
                  className: `w-full bg-gray-100 border ${(_j = actionData == null ? void 0 : actionData.errors) != null && _j.description ? "border-red-500" : "border-gray-300"} p-2 rounded-lg focus:outline-none focus:bg-white`
                },
                void 0,
                !1,
                {
                  fileName: "app/routes/products/index.tsx",
                  lineNumber: 236,
                  columnNumber: 15
                },
                this
              ),
              ((_k = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _k.description) && /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("p", { className: "text-red-500 text-sm mt-1", children: (_l = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _l.description }, void 0, !1, {
                fileName: "app/routes/products/index.tsx",
                lineNumber: 252,
                columnNumber: 17
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/products/index.tsx",
              lineNumber: 229,
              columnNumber: 13
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "grid grid-cols-2 gap-4", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "form-control", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
                  "label",
                  {
                    htmlFor: "price",
                    className: "block text-gray-700 font-bold mb-2",
                    children: "Price"
                  },
                  void 0,
                  !1,
                  {
                    fileName: "app/routes/products/index.tsx",
                    lineNumber: 259,
                    columnNumber: 17
                  },
                  this
                ),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
                  "input",
                  {
                    type: "text",
                    id: "price",
                    autoFocus: !0,
                    name: "price",
                    autoComplete: "price",
                    "aria-invalid": (_m = actionData == null ? void 0 : actionData.errors) != null && _m.price ? !0 : void 0,
                    "aria-describedby": "price-error",
                    className: "w-full bg-gray-100 border border-gray-300 p-2 rounded-lg focus:outline-none focus:bg-white"
                  },
                  void 0,
                  !1,
                  {
                    fileName: "app/routes/products/index.tsx",
                    lineNumber: 265,
                    columnNumber: 17
                  },
                  this
                ),
                ((_n = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _n.price) && /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("p", { className: "text-red-500 text-sm mt-1", children: (_o = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _o.price }, void 0, !1, {
                  fileName: "app/routes/products/index.tsx",
                  lineNumber: 276,
                  columnNumber: 19
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/products/index.tsx",
                lineNumber: 258,
                columnNumber: 15
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "form-control", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
                  "label",
                  {
                    htmlFor: "status",
                    className: "block text-gray-700 font-bold mb-2",
                    children: "Status"
                  },
                  void 0,
                  !1,
                  {
                    fileName: "app/routes/products/index.tsx",
                    lineNumber: 282,
                    columnNumber: 17
                  },
                  this
                ),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
                  "select",
                  {
                    id: "status",
                    autoFocus: !0,
                    name: "status",
                    autoComplete: "status",
                    "aria-invalid": (_p = actionData == null ? void 0 : actionData.errors) != null && _p.status ? !0 : void 0,
                    "aria-describedby": "status-error",
                    className: "w-full bg-gray-100 border border-gray-300 p-2 rounded-lg focus:outline-none focus:bg-white",
                    children: [
                      /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("option", { value: "", children: "-- Select status --" }, void 0, !1, {
                        fileName: "app/routes/products/index.tsx",
                        lineNumber: 297,
                        columnNumber: 19
                      }, this),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("option", { value: "AVAILABLE", children: "Available" }, void 0, !1, {
                        fileName: "app/routes/products/index.tsx",
                        lineNumber: 298,
                        columnNumber: 19
                      }, this),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("option", { value: "OUT_OF_STOCK", children: "Out of stock" }, void 0, !1, {
                        fileName: "app/routes/products/index.tsx",
                        lineNumber: 299,
                        columnNumber: 19
                      }, this)
                    ]
                  },
                  void 0,
                  !0,
                  {
                    fileName: "app/routes/products/index.tsx",
                    lineNumber: 288,
                    columnNumber: 17
                  },
                  this
                ),
                ((_q = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _q.status) && /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("p", { className: "text-red-500 text-sm mt-1", children: (_r = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _r.status }, void 0, !1, {
                  fileName: "app/routes/products/index.tsx",
                  lineNumber: 302,
                  columnNumber: 19
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/products/index.tsx",
                lineNumber: 281,
                columnNumber: 15
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/products/index.tsx",
              lineNumber: 257,
              columnNumber: 13
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "form-control", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
                "label",
                {
                  htmlFor: "images",
                  className: "block text-gray-700 font-bold mb-2",
                  children: "Images (comma separated URLs)"
                },
                void 0,
                !1,
                {
                  fileName: "app/routes/products/index.tsx",
                  lineNumber: 309,
                  columnNumber: 15
                },
                this
              ),
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
                "input",
                {
                  type: "text",
                  id: "images",
                  autoFocus: !0,
                  name: "images",
                  autoComplete: "images",
                  "aria-invalid": (_s = actionData == null ? void 0 : actionData.errors) != null && _s.images ? !0 : void 0,
                  "aria-describedby": "images-error",
                  className: "w-full bg-gray-100 border border-gray-300 p-2 rounded-lg focus:outline-none focus:bg-white"
                },
                void 0,
                !1,
                {
                  fileName: "app/routes/products/index.tsx",
                  lineNumber: 315,
                  columnNumber: 15
                },
                this
              ),
              ((_t = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _t.images) && /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("p", { className: "text-red-500 text-sm mt-1", children: (_u = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _u.images }, void 0, !1, {
                fileName: "app/routes/products/index.tsx",
                lineNumber: 326,
                columnNumber: 17
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/products/index.tsx",
              lineNumber: 308,
              columnNumber: 13
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(
              "button",
              {
                type: "submit",
                className: "bg-rose-900 hover:bg-rose-700 text-white font-bold py-2 px-4 my-6 rounded focus:outline-none focus:shadow-outline",
                children: "Add Product"
              },
              void 0,
              !1,
              {
                fileName: "app/routes/products/index.tsx",
                lineNumber: 331,
                columnNumber: 13
              },
              this
            )
          ] }, void 0, !0, {
            fileName: "app/routes/products/index.tsx",
            lineNumber: 165,
            columnNumber: 11
          }, this),
          " "
        ]
      },
      void 0,
      !0,
      {
        fileName: "app/routes/products/index.tsx",
        lineNumber: 160,
        columnNumber: 9
      },
      this
    ),
    !productsLoading && !errorProducts && /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("section", { className: "py-10 bg-gray-100", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "mx-auto grid max-w-7xl grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 p-6", children: productsToMap == null ? void 0 : productsToMap.map((product) => /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(import_react9.Link, { to: product == null ? void 0 : product._id, children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(Product_default, { userId, product }, void 0, !1, {
      fileName: "app/routes/products/index.tsx",
      lineNumber: 347,
      columnNumber: 19
    }, this) }, product == null ? void 0 : product._id, !1, {
      fileName: "app/routes/products/index.tsx",
      lineNumber: 346,
      columnNumber: 17
    }, this)) }, void 0, !1, {
      fileName: "app/routes/products/index.tsx",
      lineNumber: 344,
      columnNumber: 13
    }, this) }, void 0, !1, {
      fileName: "app/routes/products/index.tsx",
      lineNumber: 343,
      columnNumber: 11
    }, this) }, void 0, !1, {
      fileName: "app/routes/products/index.tsx",
      lineNumber: 342,
      columnNumber: 9
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/products/index.tsx",
    lineNumber: 131,
    columnNumber: 5
  }, this);
}, products_default = Products;

// app/routes/wishlist/index.tsx
var wishlist_exports = {};
__export(wishlist_exports, {
  default: () => wishlist_default
});
var import_client8 = require("@apollo/client"), import_react11 = require("@remix-run/react"), import_react12 = require("react");
var import_jsx_dev_runtime9 = require("react/jsx-dev-runtime"), Products2 = () => {
  let context = (0, import_react12.useContext)(auth_context_default), userId = context == null ? void 0 : context.userId, tempWishlist = getTempWishlistFromBrowser(), sortedProductIds = tempWishlist == null ? void 0 : tempWishlist.products.sort(), [mergedWishlistData, setMergedWishlistData] = (0, import_react12.useState)(
    []
  ), {
    loading: wishlistLoading,
    error: wishlistError,
    data: wishlistData
  } = (0, import_client8.useQuery)(GET_USER_WISHLIST, {
    skip: !userId,
    variables: { userId }
  }), {
    loading: anonymousWishlistLoading,
    error: anonymousWishlistError,
    data: anonymousWishlistData
  } = (0, import_client8.useQuery)(GET_ANONYMOUS_USER_WISHLIST, {
    skip: !!userId,
    variables: { productIds: sortedProductIds || [] }
  });
  (0, import_react12.useEffect)(() => {
    if (!wishlistLoading && !anonymousWishlistLoading && !wishlistError && !anonymousWishlistError) {
      let mergedData = ((wishlistData == null ? void 0 : wishlistData.userWishlist) || []).concat(
        (anonymousWishlistData == null ? void 0 : anonymousWishlistData.getProductsByIds) || []
      );
      setMergedWishlistData(mergedData);
    }
  }, [
    wishlistData,
    anonymousWishlistData,
    wishlistLoading,
    anonymousWishlistLoading,
    wishlistError,
    anonymousWishlistError
  ]);
  let memoizedMergedWishlistData = (0, import_react12.useMemo)(
    () => mergedWishlistData,
    [mergedWishlistData]
  );
  return wishlistLoading || anonymousWishlistLoading ? /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)(Spinner_default, {}, void 0, !1, {
    fileName: "app/routes/wishlist/index.tsx",
    lineNumber: 77,
    columnNumber: 12
  }, this) : wishlistError || anonymousWishlistError ? /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("p", { children: "Something went wrong" }, void 0, !1, {
    fileName: "app/routes/wishlist/index.tsx",
    lineNumber: 81,
    columnNumber: 12
  }, this) : /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("div", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("section", { className: "py-10 bg-gray-100", children: /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("div", { className: "mx-auto grid max-w-7xl grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 p-6", children: memoizedMergedWishlistData.map((product) => /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)(import_react11.Link, { to: `/products/${product == null ? void 0 : product._id}`, children: /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)(Product_default, { userId, product }, void 0, !1, {
    fileName: "app/routes/wishlist/index.tsx",
    lineNumber: 90,
    columnNumber: 15
  }, this) }, product == null ? void 0 : product._id, !1, {
    fileName: "app/routes/wishlist/index.tsx",
    lineNumber: 89,
    columnNumber: 13
  }, this)) }, void 0, !1, {
    fileName: "app/routes/wishlist/index.tsx",
    lineNumber: 87,
    columnNumber: 9
  }, this) }, void 0, !1, {
    fileName: "app/routes/wishlist/index.tsx",
    lineNumber: 86,
    columnNumber: 7
  }, this) }, void 0, !1, {
    fileName: "app/routes/wishlist/index.tsx",
    lineNumber: 85,
    columnNumber: 5
  }, this);
}, wishlist_default = Products2;

// app/routes/success/index.tsx
var success_exports = {};
__export(success_exports, {
  default: () => success_default
});
var import_jsx_dev_runtime10 = require("react/jsx-dev-runtime"), Success = () => /* @__PURE__ */ (0, import_jsx_dev_runtime10.jsxDEV)("h1", { className: "font-semibold text-lg text-center mt-4", children: "Thank you for your purchase!" }, void 0, !1, {
  fileName: "app/routes/success/index.tsx",
  lineNumber: 3,
  columnNumber: 9
}, this), success_default = Success;

// app/routes/cancel/index.tsx
var cancel_exports = {};
__export(cancel_exports, {
  default: () => cancel_default
});
var import_jsx_dev_runtime11 = require("react/jsx-dev-runtime"), Cancel = () => /* @__PURE__ */ (0, import_jsx_dev_runtime11.jsxDEV)("h1", { className: "font-semibold text-lg text-center mt-4", children: "Sorry to see you canceled your Stripe payment!" }, void 0, !1, {
  fileName: "app/routes/cancel/index.tsx",
  lineNumber: 3,
  columnNumber: 9
}, this), cancel_default = Cancel;

// app/routes/products/$id.tsx
var id_exports = {};
__export(id_exports, {
  default: () => id_default
});
var import_client9 = require("@apollo/client"), import_react13 = require("@remix-run/react");
var import_react_hot_toast2 = require("react-hot-toast");
var import_react14 = require("react");
var import_jsx_dev_runtime12 = require("react/jsx-dev-runtime"), Product2 = () => {
  let { id } = (0, import_react13.useParams)(), client = (0, import_client9.useApolloClient)(), context = (0, import_react14.useContext)(auth_context_default), { loading, error, data } = (0, import_client9.useQuery)(GET_PRODUCT, {
    variables: { productId: id, userId: context.userId }
  }), tempWishlist = getTempWishlistFromBrowser();
  if (tempWishlist && id) {
    let isProductInWishlist = tempWishlist.products.includes(id);
    anonymousUserWishlistCache(client.cache, id, isProductInWishlist);
  }
  let addProductToWishlistMutation = (0, import_client9.useMutation)(ADD_PRODUCT_TO_WISHLIST)[0], removeProductFromWishlistMutation = (0, import_client9.useMutation)(
    REMOVE_PRODUCT_FROM_WISHLIST
  )[0], addProductToCartMutation = (0, import_client9.useMutation)(ADD_PRODUCT_TO_CART)[0], wishlistButtonHandler = (product) => {
    console.log(123, product), product.isProductInWishlist ? (removeProductFromWishlist(product._id, context.userId), updateProductsCache(client.cache, product._id, !1, context == null ? void 0 : context.userId), deleteItemFromWishlistCache(client.cache, product._id, context == null ? void 0 : context.userId)) : (addProductToWishlist(product._id, context == null ? void 0 : context.userId), updateProductsCache(client.cache, product._id, !0, context == null ? void 0 : context.userId), addItemToWishlistCache(client.cache, product, context == null ? void 0 : context.userId));
  }, addProductToWishlist = async (productId, userId) => {
    if (userId) {
      let res = await addProductToWishlistMutation({
        variables: { productId, userId }
      }), { addToWishlist } = res.data;
    } else {
      let tempWishlist2 = getTempWishlistFromBrowser();
      tempWishlist2 || (tempWishlist2 = { products: [] }), tempWishlist2.products.push(productId), saveTempWishlistToBrowser(tempWishlist2), updateAnonymousUserProductsCache(client.cache, productId, !0), anonymousUserWishlistCache(client.cache, productId, !0);
    }
  }, removeProductFromWishlist = async (productId, userId) => {
    if (userId) {
      let res = await removeProductFromWishlistMutation({
        variables: { productId, userId }
      }), { removeFromWishlist } = res.data;
    } else {
      let tempWishlist2 = getTempWishlistFromBrowser();
      if (tempWishlist2) {
        updateAnonymousUserProductsCache(client.cache, productId, !1);
        let index = tempWishlist2.products.indexOf(productId);
        index !== -1 && tempWishlist2.products.splice(index, 1);
      }
      saveTempWishlistToBrowser(tempWishlist2), anonymousUserWishlistCache(client.cache, productId, !1);
    }
  }, addToCartHandler = async (productId) => {
    try {
      let promise = await addProductToCartMutation({
        variables: { productId, userId: context.userId }
      });
      console.log(555, promise), addItemToCartCache(
        client.cache,
        promise.data.addToCart,
        context == null ? void 0 : context.userId
      ), successToaster("Product added to cart!");
    } catch (error2) {
      errorToaster("Could not add product to cart."), console.error(error2);
    }
  };
  return loading ? /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)(Spinner_default, {}, void 0, !1, {
    fileName: "app/routes/products/$id.tsx",
    lineNumber: 122,
    columnNumber: 23
  }, this) : error ? /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("p", { children: "Something went wrong" }, void 0, !1, {
    fileName: "app/routes/products/$id.tsx",
    lineNumber: 123,
    columnNumber: 21
  }, this) : /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("section", { className: "py-10 bg-gray-100", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)(import_react_hot_toast2.Toaster, { position: "top-right" }, void 0, !1, {
      fileName: "app/routes/products/$id.tsx",
      lineNumber: 126,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("div", { className: "mx-auto grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 p-6", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("div", { className: "relative col-span-2 max-w-[70%] mx-auto", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)(
          "img",
          {
            id: "image",
            className: "w-full h-full object-cover",
            src: `https://source.unsplash.com/800x600/?${data.product.category ? data.product.category : "wallet"}`
          },
          void 0,
          !1,
          {
            fileName: "app/routes/products/$id.tsx",
            lineNumber: 129,
            columnNumber: 11
          },
          this
        ),
        /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("div", { className: "arrows w-full absolute inset-y-1/2 flex justify-between px-3", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("button", { id: "prev", children: /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("i", { className: "fa-solid fa-chevron-left" }, void 0, !1, {
            fileName: "app/routes/products/$id.tsx",
            lineNumber: 139,
            columnNumber: 15
          }, this) }, void 0, !1, {
            fileName: "app/routes/products/$id.tsx",
            lineNumber: 138,
            columnNumber: 13
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("button", { id: "next", children: /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("i", { className: "fa-solid fa-chevron-right" }, void 0, !1, {
            fileName: "app/routes/products/$id.tsx",
            lineNumber: 142,
            columnNumber: 15
          }, this) }, void 0, !1, {
            fileName: "app/routes/products/$id.tsx",
            lineNumber: 141,
            columnNumber: 13
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/products/$id.tsx",
          lineNumber: 137,
          columnNumber: 11
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/products/$id.tsx",
        lineNumber: 128,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("div", { className: "space-y-5 p-5", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("h4", { className: "text-xl font-semibold", children: data.product.category }, void 0, !1, {
          fileName: "app/routes/products/$id.tsx",
          lineNumber: 147,
          columnNumber: 11
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("p", { className: "text-sm", children: data.product.description }, void 0, !1, {
          fileName: "app/routes/products/$id.tsx",
          lineNumber: 150,
          columnNumber: 11
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("p", { className: "text-sm", children: data.product.status }, void 0, !1, {
          fileName: "app/routes/products/$id.tsx",
          lineNumber: 151,
          columnNumber: 11
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("div", { className: "flex items-center space-x-5", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)(
            "button",
            {
              onClick: () => addToCartHandler(data.product._id),
              className: "flex items-center space-x-2 rounded-3xl border font-bold bg-rose-900 px-5 py-2 text-white  hover:bg-white hover:border hover:border-rose-900 hover:text-rose-900",
              children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("i", { className: "fa-solid fa-cart-shopping text-xl" }, void 0, !1, {
                  fileName: "app/routes/products/$id.tsx",
                  lineNumber: 158,
                  columnNumber: 15
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("span", { children: "Add To Cart" }, void 0, !1, {
                  fileName: "app/routes/products/$id.tsx",
                  lineNumber: 159,
                  columnNumber: 15
                }, this)
              ]
            },
            void 0,
            !0,
            {
              fileName: "app/routes/products/$id.tsx",
              lineNumber: 154,
              columnNumber: 13
            },
            this
          ),
          /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)(
            "button",
            {
              onClick: () => wishlistButtonHandler(data.product),
              className: `rounded-full ${data.product.isProductInWishlist ? "bg-rose-900" : "bg-white"} hover:bg-rose-900 border-2 border-rose-900 hover:border-rose-900 p-3`,
              children: /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)(
                "svg",
                {
                  fill: "#000000",
                  width: "32px",
                  height: "32px",
                  viewBox: "-4.8 -4.8 41.60 41.60",
                  xmlns: "http://www.w3.org/2000/svg",
                  children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("g", { id: "SVGRepo_bgCarrier", strokeWidth: "0" }, void 0, !1, {
                      fileName: "app/routes/products/$id.tsx",
                      lineNumber: 174,
                      columnNumber: 17
                    }, this),
                    /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)(
                      "g",
                      {
                        id: "SVGRepo_tracerCarrier",
                        strokeLinecap: "round",
                        strokeLinejoin: "round"
                      },
                      void 0,
                      !1,
                      {
                        fileName: "app/routes/products/$id.tsx",
                        lineNumber: 175,
                        columnNumber: 17
                      },
                      this
                    ),
                    /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("g", { id: "SVGRepo_iconCarrier", children: /* @__PURE__ */ (0, import_jsx_dev_runtime12.jsxDEV)("path", { d: "M 9.5 5 C 5.363281 5 2 8.402344 2 12.5 C 2 13.929688 2.648438 15.167969 3.25 16.0625 C 3.851563 16.957031 4.46875 17.53125 4.46875 17.53125 L 15.28125 28.375 L 16 29.09375 L 16.71875 28.375 L 27.53125 17.53125 C 27.53125 17.53125 30 15.355469 30 12.5 C 30 8.402344 26.636719 5 22.5 5 C 19.066406 5 16.855469 7.066406 16 7.9375 C 15.144531 7.066406 12.933594 5 9.5 5 Z M 9.5 7 C 12.488281 7 15.25 9.90625 15.25 9.90625 L 16 10.75 L 16.75 9.90625 C 16.75 9.90625 19.511719 7 22.5 7 C 25.542969 7 28 9.496094 28 12.5 C 28 14.042969 26.125 16.125 26.125 16.125 L 16 26.25 L 5.875 16.125 C 5.875 16.125 5.390625 15.660156 4.90625 14.9375 C 4.421875 14.214844 4 13.273438 4 12.5 C 4 9.496094 6.457031 7 9.5 7 Z" }, void 0, !1, {
                      fileName: "app/routes/products/$id.tsx",
                      lineNumber: 181,
                      columnNumber: 19
                    }, this) }, void 0, !1, {
                      fileName: "app/routes/products/$id.tsx",
                      lineNumber: 180,
                      columnNumber: 17
                    }, this)
                  ]
                },
                void 0,
                !0,
                {
                  fileName: "app/routes/products/$id.tsx",
                  lineNumber: 167,
                  columnNumber: 15
                },
                this
              )
            },
            void 0,
            !1,
            {
              fileName: "app/routes/products/$id.tsx",
              lineNumber: 161,
              columnNumber: 13
            },
            this
          )
        ] }, void 0, !0, {
          fileName: "app/routes/products/$id.tsx",
          lineNumber: 153,
          columnNumber: 11
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/products/$id.tsx",
        lineNumber: 146,
        columnNumber: 9
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/products/$id.tsx",
      lineNumber: 127,
      columnNumber: 7
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/products/$id.tsx",
    lineNumber: 125,
    columnNumber: 5
  }, this);
}, id_default = Product2;

// app/routes/search/index.tsx
var search_exports = {};
__export(search_exports, {
  default: () => search_default
});
var import_client10 = require("@apollo/client"), import_react15 = require("@remix-run/react"), import_react16 = require("react");
var import_react17 = require("@remix-run/react"), import_jsx_dev_runtime13 = require("react/jsx-dev-runtime"), Products3 = () => {
  let [searchParams] = (0, import_react17.useSearchParams)(), client = (0, import_client10.useApolloClient)(), context = (0, import_react16.useContext)(auth_context_default), userId = context == null ? void 0 : context.userId, query = searchParams.get("query"), [searchProducts, { data, loading, error }] = (0, import_client10.useLazyQuery)(SEARCH_PRODUCTS), [products, setProducts] = (0, import_react16.useState)([]);
  (0, import_react16.useEffect)(() => {
    fetchProducts();
  }, [query]);
  let fetchProducts = async () => {
    try {
      if (!query)
        return;
      let productsData = client.readQuery({
        query: GET_PRODUCTS,
        variables: {
          userId
        }
      }), filteredProducts = [];
      productsData ? filteredProducts = productsData.products.filter(
        (product) => product.name.toLowerCase().includes(query.toLowerCase()) || product.description.toLowerCase().includes(query.toLowerCase())
      ) : filteredProducts = (await searchProducts({
        variables: { query }
      })).data.searchProducts, setProducts(filteredProducts), console.log(123, 1, filteredProducts);
    } catch (error2) {
      console.error("Error fetching products:", error2);
    }
  };
  if (loading)
    return /* @__PURE__ */ (0, import_jsx_dev_runtime13.jsxDEV)(Spinner_default, {}, void 0, !1, {
      fileName: "app/routes/search/index.tsx",
      lineNumber: 62,
      columnNumber: 23
    }, this);
  if (error)
    return /* @__PURE__ */ (0, import_jsx_dev_runtime13.jsxDEV)("p", { children: "Something went wrong" }, void 0, !1, {
      fileName: "app/routes/search/index.tsx",
      lineNumber: 63,
      columnNumber: 21
    }, this);
  if (products)
    return /* @__PURE__ */ (0, import_jsx_dev_runtime13.jsxDEV)("div", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime13.jsxDEV)("section", { className: "py-10 bg-gray-100", children: /* @__PURE__ */ (0, import_jsx_dev_runtime13.jsxDEV)("div", { className: "mx-auto grid max-w-7xl grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 p-6", children: products.map((product) => /* @__PURE__ */ (0, import_jsx_dev_runtime13.jsxDEV)(import_react15.Link, { to: `/products/${product == null ? void 0 : product._id}`, children: /* @__PURE__ */ (0, import_jsx_dev_runtime13.jsxDEV)(Product_default, { userId, product }, void 0, !1, {
      fileName: "app/routes/search/index.tsx",
      lineNumber: 71,
      columnNumber: 17
    }, this) }, product == null ? void 0 : product._id, !1, {
      fileName: "app/routes/search/index.tsx",
      lineNumber: 70,
      columnNumber: 15
    }, this)) }, void 0, !1, {
      fileName: "app/routes/search/index.tsx",
      lineNumber: 68,
      columnNumber: 11
    }, this) }, void 0, !1, {
      fileName: "app/routes/search/index.tsx",
      lineNumber: 67,
      columnNumber: 9
    }, this) }, void 0, !1, {
      fileName: "app/routes/search/index.tsx",
      lineNumber: 66,
      columnNumber: 7
    }, this);
}, search_default = Products3;

// app/routes/auth/index.tsx
var auth_exports = {};
__export(auth_exports, {
  action: () => action2,
  default: () => auth_default
});
var Z2 = __toESM(require("zod")), import_react18 = require("react"), import_node2 = require("@remix-run/node"), import_react19 = require("@remix-run/react");
var import_client12 = require("@apollo/client");

// app/api/users.ts
var import_client11 = require("@apollo/client"), GET_USERS = import_client11.gql`
  query Query {
    users {
      id
      name
      email
      products {
        name
        images
        price
      }
    }
  }
`, GET_USER = import_client11.gql`
  query Query($userId: ID!) {
    user(id: $userId) {
      email
    }
  }
`, LOGIN = import_client11.gql`
  mutation Mutation($loginInput: LoginInput) {
    loginUser(loginInput: $loginInput) {
      userId
      token
      tokenExpiration
      role
    }
  }
`, REGISTER = import_client11.gql`
  mutation Mutation($registerInput: RegisterInput) {
    registerUser(registerInput: $registerInput) {
      _id
      email
    }
  }
`;

// app/routes/auth/index.tsx
var import_react20 = require("@remix-run/react");
var import_jsx_dev_runtime14 = require("react/jsx-dev-runtime"), schema2 = Z2.object({
  email: Z2.string({
    required_error: "Email is required"
  }).email("Invalid email"),
  password: Z2.string().min(6, "Password must be at least 6 characters long")
}), action2 = async ({ request }) => {
  let { formData, errors } = await validationAction({
    request,
    schema: schema2
  });
  return errors ? (0, import_node2.json)({ errors }, { status: 400 }) : formData;
}, Auth = () => {
  var _a, _b, _c, _d;
  let context = (0, import_react18.useContext)(auth_context_default), loginUserMutation = (0, import_client12.useMutation)(LOGIN)[0], registerUserMutation = (0, import_client12.useMutation)(REGISTER)[0], navigate = (0, import_react20.useNavigate)(), actionData = (0, import_react19.useActionData)(), emailRef = (0, import_react18.useRef)(null), passwordRef = (0, import_react18.useRef)(null), [isLogin, setIsLogin] = (0, import_react18.useState)(!0), toggleIsLogin = () => {
    setIsLogin(!isLogin);
  }, submitHandler = (actionData2) => {
    let userInput = {
      email: actionData2 == null ? void 0 : actionData2.email,
      password: actionData2 == null ? void 0 : actionData2.password
    };
    isLogin ? loginUser(userInput) : registerUser(userInput);
  }, loginUser = async (loginInput) => {
    try {
      let res = await loginUserMutation({
        variables: { loginInput }
      });
      setAccessToken(res.data.loginUser.token), context.login(
        res.data.loginUser.token,
        res.data.loginUser.userId,
        res.data.loginUser.tokenExpiration,
        res.data.loginUser.role
      ), navigate("/user");
    } catch (error) {
      console.error(error);
    }
  }, registerUser = async (registerInput) => {
    try {
      await registerUserMutation({
        variables: { registerInput }
      }), loginUser(registerInput);
    } catch (error) {
      console.error(error);
    }
  };
  return (0, import_react18.useEffect)(() => {
    var _a2, _b2, _c2, _d2;
    (_a2 = actionData == null ? void 0 : actionData.errors) != null && _a2.email ? (_b2 = emailRef.current) == null || _b2.focus() : (_c2 = actionData == null ? void 0 : actionData.errors) != null && _c2.password && ((_d2 = passwordRef.current) == null || _d2.focus()), actionData && !(actionData != null && actionData.errors) && submitHandler(actionData);
  }, [actionData]), /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)("div", { className: "flex flex-col items-center justify-center h-screen", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)("p", { className: "text-gray-900 text-7xl font-bold", children: isLogin ? "Login" : "Register" }, void 0, !1, {
      fileName: "app/routes/auth/index.tsx",
      lineNumber: 114,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)(import_react19.Form, { className: "w-8/12 max-w-md", method: "post", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)("div", { className: "form-control", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)("label", { className: "block text-gray-700 font-bold mb-2", htmlFor: "email", children: "Email" }, void 0, !1, {
          fileName: "app/routes/auth/index.tsx",
          lineNumber: 119,
          columnNumber: 11
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)(
          "input",
          {
            ref: emailRef,
            id: "email",
            required: !0,
            autoFocus: !0,
            name: "email",
            type: "email",
            autoComplete: "email",
            "aria-invalid": (_a = actionData == null ? void 0 : actionData.errors) != null && _a.email ? !0 : void 0,
            "aria-describedby": "email-error",
            className: "appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          },
          void 0,
          !1,
          {
            fileName: "app/routes/auth/index.tsx",
            lineNumber: 122,
            columnNumber: 11
          },
          this
        ),
        ((_b = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _b.email) && /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)("div", { className: "pt-1 text-red-700", id: "email-error", children: actionData.errors.email }, void 0, !1, {
          fileName: "app/routes/auth/index.tsx",
          lineNumber: 135,
          columnNumber: 13
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/auth/index.tsx",
        lineNumber: 118,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)("div", { className: "form-control", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)(
          "label",
          {
            className: "block text-gray-700 font-bold mb-2",
            htmlFor: "password",
            children: "Password"
          },
          void 0,
          !1,
          {
            fileName: "app/routes/auth/index.tsx",
            lineNumber: 141,
            columnNumber: 11
          },
          this
        ),
        /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)(
          "input",
          {
            id: "password",
            ref: passwordRef,
            name: "password",
            type: "password",
            autoComplete: "current-password",
            "aria-invalid": (_c = actionData == null ? void 0 : actionData.errors) != null && _c.password ? !0 : void 0,
            "aria-describedby": "password-error",
            className: "appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          },
          void 0,
          !1,
          {
            fileName: "app/routes/auth/index.tsx",
            lineNumber: 147,
            columnNumber: 11
          },
          this
        ),
        ((_d = actionData == null ? void 0 : actionData.errors) == null ? void 0 : _d.password) && /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)("div", { className: "pt-1 text-red-700", id: "password-error", children: actionData.errors.password }, void 0, !1, {
          fileName: "app/routes/auth/index.tsx",
          lineNumber: 158,
          columnNumber: 13
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/auth/index.tsx",
        lineNumber: 140,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)("div", { className: "flex items-center justify-evenly mt-2", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)(
          "button",
          {
            className: "bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline",
            type: "submit",
            children: "Submit"
          },
          void 0,
          !1,
          {
            fileName: "app/routes/auth/index.tsx",
            lineNumber: 164,
            columnNumber: 11
          },
          this
        ),
        /* @__PURE__ */ (0, import_jsx_dev_runtime14.jsxDEV)(
          "button",
          {
            className: "bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline",
            type: "button",
            onClick: toggleIsLogin,
            children: isLogin ? "Sign up" : "Login"
          },
          void 0,
          !1,
          {
            fileName: "app/routes/auth/index.tsx",
            lineNumber: 170,
            columnNumber: 11
          },
          this
        )
      ] }, void 0, !0, {
        fileName: "app/routes/auth/index.tsx",
        lineNumber: 163,
        columnNumber: 9
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/auth/index.tsx",
      lineNumber: 117,
      columnNumber: 7
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/auth/index.tsx",
    lineNumber: 113,
    columnNumber: 5
  }, this);
}, auth_default = Auth;

// app/routes/cart/index.tsx
var cart_exports = {};
__export(cart_exports, {
  default: () => cart_default
});
var import_client14 = require("@apollo/client"), import_react24 = require("react");

// app/components/Cart/CartList.tsx
var import_react22 = require("@remix-run/react"), import_react23 = require("react");

// app/components/Cart/CartItem.tsx
var import_client13 = require("@apollo/client"), import_react21 = require("react");
var import_jsx_dev_runtime15 = require("react/jsx-dev-runtime"), CartItem = ({
  product,
  quantity,
  user
}) => {
  let client = (0, import_client13.useApolloClient)(), userId = user, [loading, setLoading] = (0, import_react21.useState)(!1), [productQuantity, setProductQuantity] = (0, import_react21.useState)(quantity), [productTotalPrice, setProductTotalPrice] = (0, import_react21.useState)(
    +product.price * quantity
  ), addProductToCartMutation = (0, import_client13.useMutation)(ADD_PRODUCT_TO_CART)[0], removeProductFromCartMutation = (0, import_client13.useMutation)(
    REMOVE_PRODUCT_FROM_CART
  )[0], deleteProductFromCartMutation = (0, import_client13.useMutation)(
    DELETE_PRODUCT_FROM_CART
  )[0], addToCartHandler = async (productId) => {
    try {
      setLoading(!0);
      let promise = await addProductToCartMutation({
        variables: { productId, userId }
      });
      setProductQuantity((prevQuantity) => prevQuantity + 1), setProductTotalPrice(
        (prevTotalPrice) => promise.data.addToCart.quantity * +promise.data.addToCart.product.price
      ), updateCartItemQuantityCache(
        client.cache,
        productId,
        promise.data.addToCart.quantity,
        userId
      ), setLoading(!1);
    } catch (error) {
      console.error(error), setLoading(!1);
    }
  }, removeFromCartHandler = async (productId) => {
    try {
      setLoading(!0);
      let promise = await removeProductFromCartMutation({
        variables: { productId, userId }
      });
      setProductQuantity((prevQuantity) => prevQuantity - 1), setProductTotalPrice(
        (prevTotalPrice) => promise.data.removeFromCart.quantity * +promise.data.removeFromCart.product.price
      ), updateCartItemQuantityCache(
        client.cache,
        productId,
        promise.data.removeFromCart.quantity,
        userId
      ), setLoading(!1);
    } catch (error) {
      console.error(error), setLoading(!1);
    }
  }, deleteProductHandler = async (productId) => {
    try {
      let promise = await deleteProductFromCartMutation({
        variables: { productId, userId }
      });
      deleteItemFromCartCache(client.cache, productId, userId), deleteCartItemFromProductsCache(client.cache, productId, !1, userId);
    } catch (error) {
      console.error(error);
    }
  };
  return /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)("div", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)("div", { className: "flex items-center hover:bg-gray-100 -mx-8 px-6 py-5", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)("div", { className: "flex w-2/5", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)("div", { className: "w-20", children: /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)(
        "img",
        {
          className: "h-24",
          src: `https://source.unsplash.com/800x600/?${product.category ? product.category : "wallet"}`,
          alt: ""
        },
        void 0,
        !1,
        {
          fileName: "app/components/Cart/CartItem.tsx",
          lineNumber: 103,
          columnNumber: 13
        },
        this
      ) }, void 0, !1, {
        fileName: "app/components/Cart/CartItem.tsx",
        lineNumber: 102,
        columnNumber: 11
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)("div", { className: "flex flex-col justify-center ml-4 flex-grow", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)("span", { className: "font-bold text-sm", children: [
          product.name,
          " ",
          product.description
        ] }, void 0, !0, {
          fileName: "app/components/Cart/CartItem.tsx",
          lineNumber: 112,
          columnNumber: 13
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)("span", { className: "text-center w-1/5 font-semibold text-sm", children: typeof product.price == "number" ? product.price.toFixed(2) : product.price }, void 0, !1, {
          fileName: "app/components/Cart/CartItem.tsx",
          lineNumber: 115,
          columnNumber: 13
        }, this)
      ] }, void 0, !0, {
        fileName: "app/components/Cart/CartItem.tsx",
        lineNumber: 111,
        columnNumber: 11
      }, this)
    ] }, void 0, !0, {
      fileName: "app/components/Cart/CartItem.tsx",
      lineNumber: 101,
      columnNumber: 9
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)("div", { className: "flex justify-center w-1/5", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)(
        "svg",
        {
          className: "fill-current text-gray-600 w-3 cursor-pointer",
          viewBox: "0 0 448 512",
          onClick: () => {
            productQuantity === 1 ? deleteProductHandler(product._id) : removeFromCartHandler(product._id);
          },
          children: /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)("path", { d: "M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z" }, void 0, !1, {
            fileName: "app/components/Cart/CartItem.tsx",
            lineNumber: 132,
            columnNumber: 13
          }, this)
        },
        void 0,
        !1,
        {
          fileName: "app/components/Cart/CartItem.tsx",
          lineNumber: 123,
          columnNumber: 11
        },
        this
      ),
      /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)(
        "input",
        {
          onChange: () => {
          },
          className: "mx-2 border text-center w-8",
          type: "text",
          value: productQuantity
        },
        void 0,
        !1,
        {
          fileName: "app/components/Cart/CartItem.tsx",
          lineNumber: 135,
          columnNumber: 11
        },
        this
      ),
      /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)(
        "svg",
        {
          className: "fill-current text-gray-600 w-3 cursor-pointer",
          viewBox: "0 0 448 512",
          onClick: () => addToCartHandler(product._id),
          children: /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)("path", { d: "M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z" }, void 0, !1, {
            fileName: "app/components/Cart/CartItem.tsx",
            lineNumber: 147,
            columnNumber: 13
          }, this)
        },
        void 0,
        !1,
        {
          fileName: "app/components/Cart/CartItem.tsx",
          lineNumber: 142,
          columnNumber: 11
        },
        this
      )
    ] }, void 0, !0, {
      fileName: "app/components/Cart/CartItem.tsx",
      lineNumber: 122,
      columnNumber: 9
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)(
      "div",
      {
        className: "cursor-pointer",
        onClick: () => deleteProductHandler(product._id),
        children: /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)(
          "svg",
          {
            xmlns: "http://www.w3.org/2000/svg",
            viewBox: "0 0 16 16",
            width: "16",
            height: "16",
            children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)(
                "path",
                {
                  d: "M14 3h-3.53a3.07 3.07 0 00-.6-1.65C9.44.82 8.8.5 8 .5s-1.44.32-1.87.85A3.06 3.06 0 005.53 3H2a.5.5 0 000 1h1.25v10c0 .28.22.5.5.5h8.5a.5.5 0 00.5-.5V4H14a.5.5 0 000-1zM6.91 1.98c.23-.29.58-.48 1.09-.48s.85.19 1.09.48c.2.24.3.6.36 1.02h-2.9c.05-.42.17-.78.36-1.02zm4.84 11.52h-7.5V4h7.5v9.5z",
                  fill: "currentColor"
                },
                void 0,
                !1,
                {
                  fileName: "app/components/Cart/CartItem.tsx",
                  lineNumber: 160,
                  columnNumber: 13
                },
                this
              ),
              /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)(
                "path",
                {
                  d: "M6.55 5.25a.5.5 0 00-.5.5v6a.5.5 0 001 0v-6a.5.5 0 00-.5-.5zM9.45 5.25a.5.5 0 00-.5.5v6a.5.5 0 001 0v-6a.5.5 0 00-.5-.5z",
                  fill: "currentColor"
                },
                void 0,
                !1,
                {
                  fileName: "app/components/Cart/CartItem.tsx",
                  lineNumber: 164,
                  columnNumber: 13
                },
                this
              )
            ]
          },
          void 0,
          !0,
          {
            fileName: "app/components/Cart/CartItem.tsx",
            lineNumber: 154,
            columnNumber: 11
          },
          this
        )
      },
      void 0,
      !1,
      {
        fileName: "app/components/Cart/CartItem.tsx",
        lineNumber: 150,
        columnNumber: 9
      },
      this
    ),
    loading ? /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)("span", { className: "text-center w-1/5 font-semibold text-sm", children: /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)(Spinner_default, {}, void 0, !1, {
      fileName: "app/components/Cart/CartItem.tsx",
      lineNumber: 172,
      columnNumber: 13
    }, this) }, void 0, !1, {
      fileName: "app/components/Cart/CartItem.tsx",
      lineNumber: 171,
      columnNumber: 11
    }, this) : /* @__PURE__ */ (0, import_jsx_dev_runtime15.jsxDEV)("span", { className: "text-center w-1/5 font-semibold text-sm", children: productTotalPrice.toFixed(2) }, void 0, !1, {
      fileName: "app/components/Cart/CartItem.tsx",
      lineNumber: 175,
      columnNumber: 11
    }, this)
  ] }, void 0, !0, {
    fileName: "app/components/Cart/CartItem.tsx",
    lineNumber: 100,
    columnNumber: 7
  }, this) }, void 0, !1, {
    fileName: "app/components/Cart/CartItem.tsx",
    lineNumber: 99,
    columnNumber: 5
  }, this);
}, CartItem_default = CartItem;

// app/components/Cart/CartList.tsx
var import_jsx_dev_runtime16 = require("react/jsx-dev-runtime"), CartList = (props) => {
  var _a;
  let [total, setTotal] = (0, import_react23.useState)(0), userId = props == null ? void 0 : props.userId, cartItems = (_a = props == null ? void 0 : props.cartDataProps) == null ? void 0 : _a.userCartItems;
  (0, import_react23.useEffect)(() => {
    let totalPrice = cartItems == null ? void 0 : cartItems.reduce(
      (accumulator, currentItem) => {
        var _a2;
        let itemTotal = ((_a2 = currentItem == null ? void 0 : currentItem.product) == null ? void 0 : _a2.price) * (currentItem == null ? void 0 : currentItem.quantity);
        return accumulator + itemTotal;
      },
      0
    );
    setTotal(totalPrice);
  }, [cartItems]);
  let checkout = async () => {
    await fetch("http://localhost:8002/checkout", {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ items: cartItems })
    }).then((res) => res.json()).then((res) => {
      res.url && window.location.assign(res.url);
    });
  };
  return /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("div", { className: "md:w-4/5 w-full mx-auto mt-10", children: /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("div", { className: "flex flex-col shadow-md my-10", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("div", { className: "w-full bg-white px-10 py-10", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("div", { className: "flex justify-between border-b pb-8", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("h1", { className: "font-semibold text-2xl", children: "Shopping Cart" }, void 0, !1, {
          fileName: "app/components/Cart/CartList.tsx",
          lineNumber: 44,
          columnNumber: 13
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("h2", { className: "font-semibold text-2xl", children: [
          cartItems == null ? void 0 : cartItems.length,
          " Items"
        ] }, void 0, !0, {
          fileName: "app/components/Cart/CartList.tsx",
          lineNumber: 45,
          columnNumber: 13
        }, this)
      ] }, void 0, !0, {
        fileName: "app/components/Cart/CartList.tsx",
        lineNumber: 43,
        columnNumber: 11
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("div", { className: "flex mt-10 mb-5", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("h3", { className: "font-semibold text-gray-600 text-xs uppercase w-2/5", children: "Product Details" }, void 0, !1, {
          fileName: "app/components/Cart/CartList.tsx",
          lineNumber: 50,
          columnNumber: 13
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("h3", { className: "font-semibold text-center text-gray-600 text-xs uppercase w-1/5", children: "Quantity" }, void 0, !1, {
          fileName: "app/components/Cart/CartList.tsx",
          lineNumber: 53,
          columnNumber: 13
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("h3", { className: "font-semibold text-center text-gray-600 text-xs uppercase w-1/5", children: "Total" }, void 0, !1, {
          fileName: "app/components/Cart/CartList.tsx",
          lineNumber: 56,
          columnNumber: 13
        }, this)
      ] }, void 0, !0, {
        fileName: "app/components/Cart/CartList.tsx",
        lineNumber: 49,
        columnNumber: 11
      }, this),
      cartItems == null ? void 0 : cartItems.map((product) => /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)(
        CartItem_default,
        {
          product: product == null ? void 0 : product.product,
          quantity: product == null ? void 0 : product.quantity,
          user: userId
        },
        product == null ? void 0 : product.product._id,
        !1,
        {
          fileName: "app/components/Cart/CartList.tsx",
          lineNumber: 61,
          columnNumber: 13
        },
        this
      )),
      /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)(
        import_react22.Link,
        {
          to: "/products",
          className: "flex font-semibold text-indigo-600 text-sm mt-10",
          children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)(
              "svg",
              {
                className: "fill-current mr-2 text-indigo-600 w-4",
                viewBox: "0 0 448 512",
                children: /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("path", { d: "M134.059 296H436c6.627 0 12-5.373 12-12v-56c0-6.627-5.373-12-12-12H134.059v-46.059c0-21.382-25.851-32.09-40.971-16.971L7.029 239.029c-9.373 9.373-9.373 24.569 0 33.941l86.059 86.059c15.119 15.119 40.971 4.411 40.971-16.971V296z" }, void 0, !1, {
                  fileName: "app/components/Cart/CartList.tsx",
                  lineNumber: 76,
                  columnNumber: 15
                }, this)
              },
              void 0,
              !1,
              {
                fileName: "app/components/Cart/CartList.tsx",
                lineNumber: 72,
                columnNumber: 13
              },
              this
            ),
            "Continue Shopping"
          ]
        },
        void 0,
        !0,
        {
          fileName: "app/components/Cart/CartList.tsx",
          lineNumber: 68,
          columnNumber: 11
        },
        this
      )
    ] }, void 0, !0, {
      fileName: "app/components/Cart/CartList.tsx",
      lineNumber: 42,
      columnNumber: 9
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)(
      "div",
      {
        id: "summary",
        className: "w-2/5 sm:w-full px-4 sm:px-8 py-10 mx-auto",
        children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("h1", { className: "font-semibold text-2xl border-b pb-8", children: "Order Summary" }, void 0, !1, {
            fileName: "app/components/Cart/CartList.tsx",
            lineNumber: 86,
            columnNumber: 11
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("div", { className: "flex flex-col sm:flex-row justify-between mt-10 mb-5", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("span", { className: "font-semibold text-sm uppercase mb-2 sm:mb-0", children: [
              "Items: ",
              cartItems == null ? void 0 : cartItems.length
            ] }, void 0, !0, {
              fileName: "app/components/Cart/CartList.tsx",
              lineNumber: 90,
              columnNumber: 13
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("span", { className: "font-semibold text-sm", children: [
              "Total: ",
              total == null ? void 0 : total.toFixed(2)
            ] }, void 0, !0, {
              fileName: "app/components/Cart/CartList.tsx",
              lineNumber: 93,
              columnNumber: 13
            }, this)
          ] }, void 0, !0, {
            fileName: "app/components/Cart/CartList.tsx",
            lineNumber: 89,
            columnNumber: 11
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("div", { children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("label", { className: "font-medium inline-block mb-3 text-sm uppercase", children: "Shipping" }, void 0, !1, {
              fileName: "app/components/Cart/CartList.tsx",
              lineNumber: 98,
              columnNumber: 13
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("select", { className: "block p-2 text-gray-600 w-full text-sm", children: /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("option", { children: "Standard shipping - $10.00" }, void 0, !1, {
              fileName: "app/components/Cart/CartList.tsx",
              lineNumber: 102,
              columnNumber: 15
            }, this) }, void 0, !1, {
              fileName: "app/components/Cart/CartList.tsx",
              lineNumber: 101,
              columnNumber: 13
            }, this)
          ] }, void 0, !0, {
            fileName: "app/components/Cart/CartList.tsx",
            lineNumber: 97,
            columnNumber: 11
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("div", { className: "py-10", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)(
              "label",
              {
                htmlFor: "promo",
                className: "font-semibold inline-block mb-3 text-sm uppercase",
                children: "Promo Code"
              },
              void 0,
              !1,
              {
                fileName: "app/components/Cart/CartList.tsx",
                lineNumber: 106,
                columnNumber: 13
              },
              this
            ),
            /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)(
              "input",
              {
                type: "text",
                id: "promo",
                placeholder: "Enter your code",
                className: "p-2 text-sm w-full"
              },
              void 0,
              !1,
              {
                fileName: "app/components/Cart/CartList.tsx",
                lineNumber: 112,
                columnNumber: 13
              },
              this
            )
          ] }, void 0, !0, {
            fileName: "app/components/Cart/CartList.tsx",
            lineNumber: 105,
            columnNumber: 11
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("button", { className: "bg-red-500 hover:bg-red-600 px-5 py-2 text-sm text-white uppercase", children: "Apply" }, void 0, !1, {
            fileName: "app/components/Cart/CartList.tsx",
            lineNumber: 119,
            columnNumber: 11
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("div", { className: "border-t mt-8", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("div", { className: "flex flex-col sm:flex-row font-semibold justify-between py-6 text-sm uppercase", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("span", { children: "Total cost" }, void 0, !1, {
                fileName: "app/components/Cart/CartList.tsx",
                lineNumber: 124,
                columnNumber: 15
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)("span", { children: (total + 10).toFixed(2) }, void 0, !1, {
                fileName: "app/components/Cart/CartList.tsx",
                lineNumber: 125,
                columnNumber: 15
              }, this)
            ] }, void 0, !0, {
              fileName: "app/components/Cart/CartList.tsx",
              lineNumber: 123,
              columnNumber: 13
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime16.jsxDEV)(
              "button",
              {
                onClick: checkout,
                className: "bg-indigo-500 font-semibold hover:bg-indigo-600 py-3 text-sm text-white uppercase w-full",
                children: "Checkout"
              },
              void 0,
              !1,
              {
                fileName: "app/components/Cart/CartList.tsx",
                lineNumber: 127,
                columnNumber: 13
              },
              this
            )
          ] }, void 0, !0, {
            fileName: "app/components/Cart/CartList.tsx",
            lineNumber: 122,
            columnNumber: 11
          }, this)
        ]
      },
      void 0,
      !0,
      {
        fileName: "app/components/Cart/CartList.tsx",
        lineNumber: 82,
        columnNumber: 9
      },
      this
    )
  ] }, void 0, !0, {
    fileName: "app/components/Cart/CartList.tsx",
    lineNumber: 41,
    columnNumber: 7
  }, this) }, void 0, !1, {
    fileName: "app/components/Cart/CartList.tsx",
    lineNumber: 40,
    columnNumber: 5
  }, this);
}, CartList_default = CartList;

// app/routes/cart/index.tsx
var import_jsx_dev_runtime17 = require("react/jsx-dev-runtime"), Products4 = () => {
  let context = (0, import_react24.useContext)(auth_context_default), userId = context == null ? void 0 : context.userId, {
    loading: cartDataLoading,
    error: cartDataError,
    data: cartData
  } = (0, import_client14.useQuery)(
    GET_USER_CART_PRODUCTS,
    (0, import_react24.useMemo)(
      () => ({
        skip: !userId,
        variables: { userId }
      }),
      [context == null ? void 0 : context.userId]
    )
  );
  return cartDataLoading ? /* @__PURE__ */ (0, import_jsx_dev_runtime17.jsxDEV)(Spinner_default, {}, void 0, !1, {
    fileName: "app/routes/cart/index.tsx",
    lineNumber: 28,
    columnNumber: 31
  }, this) : cartDataError || !userId ? /* @__PURE__ */ (0, import_jsx_dev_runtime17.jsxDEV)("p", { children: "Something went wrong" }, void 0, !1, {
    fileName: "app/routes/cart/index.tsx",
    lineNumber: 29,
    columnNumber: 40
  }, this) : /* @__PURE__ */ (0, import_jsx_dev_runtime17.jsxDEV)(import_jsx_dev_runtime17.Fragment, { children: !cartDataLoading && !cartDataError && /* @__PURE__ */ (0, import_jsx_dev_runtime17.jsxDEV)(CartList_default, { userId, cartDataProps: cartData }, void 0, !1, {
    fileName: "app/routes/cart/index.tsx",
    lineNumber: 33,
    columnNumber: 9
  }, this) }, void 0, !1, {
    fileName: "app/routes/cart/index.tsx",
    lineNumber: 31,
    columnNumber: 5
  }, this);
}, cart_default = Products4;

// app/routes/user/index.tsx
var user_exports = {};
__export(user_exports, {
  default: () => user_default
});
var import_client15 = require("@apollo/client"), import_react25 = require("react");
var import_jsx_dev_runtime18 = require("react/jsx-dev-runtime");
function User() {
  let context = (0, import_react25.useContext)(auth_context_default), { data, loading, error } = (0, import_client15.useQuery)(GET_USER, {
    variables: { userId: context.userId },
    skip: !context.userId
  });
  if (loading)
    return /* @__PURE__ */ (0, import_jsx_dev_runtime18.jsxDEV)(Spinner_default, {}, void 0, !1, {
      fileName: "app/routes/user/index.tsx",
      lineNumber: 14,
      columnNumber: 23
    }, this);
  if (error)
    return /* @__PURE__ */ (0, import_jsx_dev_runtime18.jsxDEV)("p", { children: [
      "Error: ",
      error.message
    ] }, void 0, !0, {
      fileName: "app/routes/user/index.tsx",
      lineNumber: 15,
      columnNumber: 21
    }, this);
  if (data) {
    let { user } = data;
    return /* @__PURE__ */ (0, import_jsx_dev_runtime18.jsxDEV)(import_jsx_dev_runtime18.Fragment, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime18.jsxDEV)("p", { children: [
      "User email: ",
      user.email
    ] }, void 0, !0, {
      fileName: "app/routes/user/index.tsx",
      lineNumber: 20,
      columnNumber: 9
    }, this) }, void 0, !1, {
      fileName: "app/routes/user/index.tsx",
      lineNumber: 19,
      columnNumber: 7
    }, this);
  }
  return null;
}
var user_default = User;

// app/routes/index.tsx
var routes_exports = {};
__export(routes_exports, {
  default: () => Index
});
var import_jsx_dev_runtime19 = require("react/jsx-dev-runtime");
function Index() {
  return /* @__PURE__ */ (0, import_jsx_dev_runtime19.jsxDEV)(import_jsx_dev_runtime19.Fragment, {}, void 0, !1, {
    fileName: "app/routes/index.tsx",
    lineNumber: 5,
    columnNumber: 10
  }, this);
}

// server-assets-manifest:@remix-run/dev/assets-manifest
var assets_manifest_default = { version: "b1fd92d8", entry: { module: "/build/entry.client-JQFDLYQU.js", imports: ["/build/_shared/chunk-524GAXAR.js", "/build/_shared/chunk-X7TZ6AYC.js", "/build/_shared/chunk-WDL7HJRT.js", "/build/_shared/chunk-S5MSDUWV.js", "/build/_shared/chunk-FNO6YWBP.js", "/build/_shared/chunk-ELHQWBYA.js", "/build/_shared/chunk-EETRBLDB.js"] }, routes: { root: { id: "root", parentId: void 0, path: "", index: void 0, caseSensitive: void 0, module: "/build/root-VJU242DS.js", imports: ["/build/_shared/chunk-WJOHDJCD.js", "/build/_shared/chunk-IZ4HR7RT.js"], hasAction: !1, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/auth/index": { id: "routes/auth/index", parentId: "root", path: "auth", index: !0, caseSensitive: void 0, module: "/build/routes/auth/index-2G66CLPH.js", imports: ["/build/_shared/chunk-26QXZ5NF.js", "/build/_shared/chunk-RRWTPDMU.js", "/build/_shared/chunk-KD77CYLJ.js"], hasAction: !0, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/cancel/index": { id: "routes/cancel/index", parentId: "root", path: "cancel", index: !0, caseSensitive: void 0, module: "/build/routes/cancel/index-6XCMDMLR.js", imports: void 0, hasAction: !1, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/cart/index": { id: "routes/cart/index", parentId: "root", path: "cart", index: !0, caseSensitive: void 0, module: "/build/routes/cart/index-IR2NAUXX.js", imports: ["/build/_shared/chunk-7NEBAC2U.js", "/build/_shared/chunk-EXSL6QDV.js"], hasAction: !1, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/index": { id: "routes/index", parentId: "root", path: void 0, index: !0, caseSensitive: void 0, module: "/build/routes/index-XANPJDMU.js", imports: void 0, hasAction: !1, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/products/$id": { id: "routes/products/$id", parentId: "root", path: "products/:id", index: void 0, caseSensitive: void 0, module: "/build/routes/products/$id-OJPRJEU4.js", imports: ["/build/_shared/chunk-23ZYCVFZ.js", "/build/_shared/chunk-KD77CYLJ.js", "/build/_shared/chunk-7NEBAC2U.js", "/build/_shared/chunk-EXSL6QDV.js"], hasAction: !1, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/products/index": { id: "routes/products/index", parentId: "root", path: "products", index: !0, caseSensitive: void 0, module: "/build/routes/products/index-DVOMTFYI.js", imports: ["/build/_shared/chunk-RRWTPDMU.js", "/build/_shared/chunk-23ZYCVFZ.js", "/build/_shared/chunk-FQDVTIJ2.js", "/build/_shared/chunk-KD77CYLJ.js", "/build/_shared/chunk-7NEBAC2U.js", "/build/_shared/chunk-EXSL6QDV.js"], hasAction: !0, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/search/index": { id: "routes/search/index", parentId: "root", path: "search", index: !0, caseSensitive: void 0, module: "/build/routes/search/index-XEMEOHWE.js", imports: ["/build/_shared/chunk-FQDVTIJ2.js", "/build/_shared/chunk-KD77CYLJ.js", "/build/_shared/chunk-7NEBAC2U.js", "/build/_shared/chunk-EXSL6QDV.js"], hasAction: !1, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/success/index": { id: "routes/success/index", parentId: "root", path: "success", index: !0, caseSensitive: void 0, module: "/build/routes/success/index-OECKGUVV.js", imports: void 0, hasAction: !1, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/user/index": { id: "routes/user/index", parentId: "root", path: "user", index: !0, caseSensitive: void 0, module: "/build/routes/user/index-ILWYQAME.js", imports: ["/build/_shared/chunk-26QXZ5NF.js", "/build/_shared/chunk-EXSL6QDV.js"], hasAction: !1, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 }, "routes/wishlist/index": { id: "routes/wishlist/index", parentId: "root", path: "wishlist", index: !0, caseSensitive: void 0, module: "/build/routes/wishlist/index-U7PPHCEJ.js", imports: ["/build/_shared/chunk-FQDVTIJ2.js", "/build/_shared/chunk-KD77CYLJ.js", "/build/_shared/chunk-7NEBAC2U.js", "/build/_shared/chunk-EXSL6QDV.js"], hasAction: !1, hasLoader: !1, hasCatchBoundary: !1, hasErrorBoundary: !1 } }, cssBundleHref: void 0, url: "/build/manifest-B1FD92D8.js" };

// server-entry-module:@remix-run/dev/server-build
var assetsBuildDirectory = "public\\build", future = { unstable_cssModules: !1, unstable_cssSideEffectImports: !1, unstable_vanillaExtract: !1, v2_errorBoundary: !1, v2_meta: !1, v2_routeConvention: !1 }, publicPath = "/build/", entry = { module: entry_server_exports }, routes = {
  root: {
    id: "root",
    parentId: void 0,
    path: "",
    index: void 0,
    caseSensitive: void 0,
    module: root_exports
  },
  "routes/products/index": {
    id: "routes/products/index",
    parentId: "root",
    path: "products",
    index: !0,
    caseSensitive: void 0,
    module: products_exports
  },
  "routes/wishlist/index": {
    id: "routes/wishlist/index",
    parentId: "root",
    path: "wishlist",
    index: !0,
    caseSensitive: void 0,
    module: wishlist_exports
  },
  "routes/success/index": {
    id: "routes/success/index",
    parentId: "root",
    path: "success",
    index: !0,
    caseSensitive: void 0,
    module: success_exports
  },
  "routes/cancel/index": {
    id: "routes/cancel/index",
    parentId: "root",
    path: "cancel",
    index: !0,
    caseSensitive: void 0,
    module: cancel_exports
  },
  "routes/products/$id": {
    id: "routes/products/$id",
    parentId: "root",
    path: "products/:id",
    index: void 0,
    caseSensitive: void 0,
    module: id_exports
  },
  "routes/search/index": {
    id: "routes/search/index",
    parentId: "root",
    path: "search",
    index: !0,
    caseSensitive: void 0,
    module: search_exports
  },
  "routes/auth/index": {
    id: "routes/auth/index",
    parentId: "root",
    path: "auth",
    index: !0,
    caseSensitive: void 0,
    module: auth_exports
  },
  "routes/cart/index": {
    id: "routes/cart/index",
    parentId: "root",
    path: "cart",
    index: !0,
    caseSensitive: void 0,
    module: cart_exports
  },
  "routes/user/index": {
    id: "routes/user/index",
    parentId: "root",
    path: "user",
    index: !0,
    caseSensitive: void 0,
    module: user_exports
  },
  "routes/index": {
    id: "routes/index",
    parentId: "root",
    path: void 0,
    index: !0,
    caseSensitive: void 0,
    module: routes_exports
  }
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  assets,
  assetsBuildDirectory,
  entry,
  future,
  publicPath,
  routes
});
//# sourceMappingURL=index.js.map
